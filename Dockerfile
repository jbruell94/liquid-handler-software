FROM --platform=linux/arm64 python:3.9-alpine3.14

WORKDIR /app
COPY . .

WORKDIR Src/

RUN python3 -m pip install .

CMD ["python3", "web/webGui.py"]
