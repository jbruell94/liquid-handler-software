import can
import time
import sys
import struct

SETGLOBALPARAMCOMMAND = 9
STOPCOMMAND = 3
ROTATECOMMAND = 1
MAXMOTORS = 2048
GPCANREPLYID = 70
GPCANID = 71

def scanBus():
    print("\nLooking for motors on this CAN bus, this may take a minute.")

    for i in range(MAXMOTORS):
        frame = []
        tmp=struct.pack('Bxxxxxxx', STOPCOMMAND)
        for j in range(len(tmp)):
            frame.append(tmp[j])
        msg = can.Message(arbitration_id=i, data=frame, is_extended_id=False)
        try:
            bus.send(msg)
        except:
            time.sleep(.5)

    messages = []

    print("\nRecieved messages:")

    while(True):
        msg = bus.recv(.5)
        if (msg == None):
            break
        print(msg)
        messages.append(msg)

    print("\nFound {} motor(s):".format(len(messages)))

    for i in range(len(messages)):
        motorFound = False
        moduleAddress = messages[i].data[0]
        while(not(motorFound or moduleAddress >= MAXMOTORS)):
            frame = []
            tmp=struct.pack('>BxxxxHx', ROTATECOMMAND, 51200) #rotate at 51200 pps
            for j in range(len(tmp)):
                frame.append(tmp[j])
            bus.send(can.Message(arbitration_id=moduleAddress, data=frame, is_extended_id=False))
            msg = bus.recv(.5)
            if (msg == None):
                moduleAddress += 256
            else:
                motorFound = True
        print("Spinning motor {}, which has module address {} and reply address {}".format(i+1, moduleAddress, messages[i].arbitration_id))

        time.sleep(.8)
        frame = []
        tmp=struct.pack('Bxxxxxxx', STOPCOMMAND)
        for j in range(len(tmp)):
            frame.append(tmp[j])
        bus.send(can.Message(arbitration_id=moduleAddress, data=frame, is_extended_id=False))
        time.sleep(.5)
        bus.recv(.5)

def configureIDInteractive():
    validNumMotors = False

    while (not validNumMotors):
        val = input("\nEnter the number motors you would like to configure IDs: ")
        try:
            numMotors = int(val)
            validNumMotors = True
        except Exception as e:
            print("Bad number of motors, try again")

    for i in range(numMotors):

        changedModuleID = False
        changedReplyID = False
        input("\nPlug in the motor you want to configure and unplug any with the same module address, then press enter to continue.")
        while(not(changedModuleID and changedReplyID)):
            argsInitialized = False
            currentAddresses = input("Enter the current module address and reply address seperated by a space (0-2047): ").split()
            newAddresses = input("Enter the desired module address and reply address seperated by a (0-2047): ").split()
            if (len(currentAddresses) == 2 and len(newAddresses) == 2):
                try:
                    currentModuleAddress = int(currentAddresses[0])
                    currentReplyAddress = int(currentAddresses[1])
                    newModuleAddress = int(newAddresses[0])
                    newReplyAddress = int(newAddresses[1])
                    argsInitialized = True
                except Exception as e:
                    argsInitialized = False

            if (argsInitialized):
                print("Configuring reply ID...")
                try:
                    newReplyIDResponse = configureReplyID(currentModuleAddress, newReplyAddress)
                    print("Response: {}".format(newReplyIDResponse))
                    changedReplyID = True
                except ValueError as e:
                    print(e)
                except can.CanError as e:
                    print(e)

                if (changedReplyID):
                    try:
                        newModuleIDResponse = configureModuleID(currentModuleAddress, newModuleAddress)
                        print("Response: {}".format(newModuleIDResponse))
                        changedModuleID = True
                    except ValueError as e:
                        print(e)
                    except can.CanError as e:
                        print(e)

            else:
                print("Bad/incorrect number of arguments provided.")

            if (not(changedModuleID and changedReplyID)):
                print("Try again.\n")
            else:
                print("Configuration successful.")

def configureModuleID(currentModuleID: int, newModuleID: int):
    return configureID(GPCANID, currentModuleID, newModuleID)

def configureReplyID(currentModuleID: int, newReplyID: int):
    return configureID(GPCANREPLYID, currentModuleID, newReplyID)

def configureID(type: int, currentModuleID: int, newID: int):
    if (not((currentModuleID in range(MAXMOTORS)) and (newID in range(MAXMOTORS)) and (type == GPCANID or type == GPCANREPLYID))):
        raise ValueError("Given one or more argument(s) outside valid range.")

    frame = []
    tmp=struct.pack('>BBxxxHx', SETGLOBALPARAMCOMMAND, type, newID)
    for i in range(len(tmp)):
        frame.append(tmp[i])
    bus.send(can.Message(arbitration_id=currentModuleID, data=frame, is_extended_id=False))
    newIDResponse = bus.recv(.5)

    if ((newIDResponse == None) or (newID != (newIDResponse.data[6] | (newIDResponse.data[5] << 8)))):
        raise can.CanError("Configuration Failed!")

    return newIDResponse


##########################################################################################################################


try:
    bus = can.interface.Bus(channel='can0', bustype='socketcan')
    bus.send(can.Message(), .5)
except:
    print("The socketcan bus is down or not present.\n")
    quit()

if ((len(sys.argv) == 2) and (sys.argv[1] == "-interactive")):
    val = input("Enter 's' to scan for motors on this bus: ")
    if (val == 's'):
        scanBus()

    val = input("\nEnter 'q' if you would you like quit without reconfiguring motor ID's: ")

    if (val.lower() == 'q'):
        print("Exiting script without modifying IDs")
        quit()

    configureIDInteractive()

    print("\nConfiguration complete!")


elif ((len(sys.argv) == 2) and (sys.argv[1] == "-scan")):
    scanBus()

elif ((len(sys.argv) == 4) and (sys.argv[1] == "-configureModuleID")):
    try:
        print(configureModuleID(int(sys.argv[2]), int(sys.argv[3])))
        print("Configuration successful.")
    except ValueError as e:
        print(e)
    except can.CanError as e:
        print(e)

elif ((len(sys.argv) == 4) and (sys.argv[1] == "-configureReplyID")):
    try:
        print(configureReplyID(int(sys.argv[2]), int(sys.argv[3])))
        print("Configuration successful.")
    except ValueError as e:
        print(e)
    except can.CanError as e:
        print(e)

elif ((len(sys.argv) == 2) and (sys.argv[1] == "-help")):
    print("-help: displays command list.")
    print("-interactive: launches interactive motor configuration.")
    print("-scan: scans the CAN bus for motors, spins them, and displays their IDs.")
    print("-configureModuleID <current module ID> <new module ID>: changes the given modules ID to the new one.")
    print("-configureReplyID <current module ID> <new reply ID>: changes the given modules reply ID to the new one.")

else:
    print("Invalid args, use -help for usage information.")

print("")

