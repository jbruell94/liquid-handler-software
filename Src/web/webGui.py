from flask import Flask, render_template, request
from flask import flash
from werkzeug.utils import secure_filename
import os
from datetime import datetime
import json
from  hal.liquidHandlerHal import LiquidHandlerHal
from opentrons.liquidHandlerAL import LiquidHandlerIal


ApplicationHandler = object
ApplicationHandlerInitialized = False
HTML = "festo3_0.html"

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

InstructionAbstrationHandler = object

@app.route('/', methods=["POST", "GET"])
def index():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if (ApplicationHandlerInitialized == False):
        ApplicationHandler = LiquidHandlerHal()
        ApplicationHandlerInitialized = True
        
    ApplicationHandler.DumpAxisRampSettings('x')
    ApplicationHandler.DumpAxisRampSettings('y')
    flash("Please Home the GANTRY")

    return render_template(HTML)

@app.route("/upload", methods = ['GET', 'POST'])
def upload_file():
    global HTML
    global InstructionAbstrationHandler

    if request.method == 'POST':
        if (len(request.files) > 0):
            
            InstructionAbstrationHandler = LiquidHandlerIal()

            f = request.files['input-b2']
            secureName = secure_filename(f.filename)
            dt = datetime.now()
            dateTimeStr = "{0}_{1}_{2}_{3}_{4}".format(dt.strftime("%d"), dt.strftime("%m"), dt.strftime("%y"),
                                                    dt.strftime("%H"), dt.strftime("%M"))
            secureName = "{0}-{1}".format(dateTimeStr, secureName)

            fullPath = os.path.join('..', '..', 'json_files', secureName)
            f.save(fullPath)
            print(f'file stored to {fullPath}')

            flash(f'Application Start! Time[{dateTimeStr}] ', 'alert')
            InstructionAbstrationHandler.runApplication(fullPath)
            InstructionAbstrationHandler.waitForApplicationComplete()
            
            dt = datetime.now()
            dateTimeStr = "{0}_{1}_{2}_{3}_{4}".format(dt.strftime("%d"), dt.strftime("%m"), dt.strftime("%y"),
                                                    dt.strftime("%H"), dt.strftime("%M"))
            flash(f'Application Done! Time[{dateTimeStr}]', 'alert')


            # flash('Functionality under development!', 'alert')
        else:
            flash('Warning: No file selected! Please select file first!')

        return render_template(HTML)


@app.route('/stopJson', methods=["POST", "GET"])
def stopJson():
    global InstructionAbstrationHandler
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        # a = request.form['btnStop']
        InstructionAbstrationHandler.stopApplication()
        flash('Application stopped!', 'alert')

    return render_template(HTML)


@app.route('/move', methods=["POST", "GET"])
def move():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        req = request.form
        xAxis = request.form['xA']
        yAxis = request.form['yA']
        zAxis = request.form['zA']

        print(req)
        print(xAxis, yAxis, zAxis)

        x = y = z = -1

        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        if ApplicationHandler.SUCCESSFUL_HOMING == True:    
            if len(zAxis) != 0:
                z = float(zAxis)
                ApplicationHandler.MoveAxis("z", float(zAxis))
                print(z)

            if len(xAxis) != 0:
                x = float(xAxis)
                ApplicationHandler.MoveAxis("x", float(xAxis))

            if len(yAxis) != 0:
                y = float(yAxis)
                ApplicationHandler.MoveAxis("y", float(yAxis))

            print(x, y, z)
        else:
            print("Please Home the System!!!")
            flash("Please Home the System!!!")

    return render_template(HTML)

@app.route('/home', methods=["POST", "GET"])
def home():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        a = request.form['btnHome']

        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        ApplicationHandler.HomeAllAxises()

    return render_template(HTML)

@app.route('/home_zero', methods=["POST", "GET"])
def home_zero():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        a = request.form['btnHomeZero']

        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        ApplicationHandler.ZeroAllAxises()
        # ApplicationHandler.MotorMoveXYZ(0,0,0)

    return render_template(HTML)

@app.route('/aspirate', methods=['POST', "GET"])
def aspirate():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        asp_vol = request.form['formAsp']
        asp_vol = int(asp_vol)
        
        if asp_vol in range(0, 30):
            if ApplicationHandlerInitialized == False:
                ApplicationHandler = LiquidHandlerHal()
                ApplicationHandlerInitialized = True

            print(asp_vol)
            ApplicationHandler.Aspirate(int(asp_vol))
        else:
            #return error statement for wrong vol
            pass
    return render_template(HTML)

@app.route('/dispense', methods=['POST', "GET"])
def dispense():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        disp_vol = request.form['formDisp']
        disp_vol = int(disp_vol)
        if disp_vol in range(0, 30):
            if ApplicationHandlerInitialized == False:
                ApplicationHandler = LiquidHandlerHal()
                ApplicationHandlerInitialized = True

            print(disp_vol)
            ApplicationHandler.Dispense(disp_vol)
        else:
            #return error statement for wrong vol
            pass

    return render_template(HTML)

@app.route('/moveAxisX', methods=['POST', "GET"])
def moveAxisXUpward():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        buttonState = request.form['javascript_data']
        
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        if (buttonState == 'pressForward'):
            # print(f"Move forward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('x', ApplicationHandler.jogDirections.Forward)
        elif (buttonState == 'pressBackword'):
            # print(f"Move backward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('x', ApplicationHandler.jogDirections.Backward)
        elif (buttonState == 'releaseForward' or buttonState == 'releaseBackword'):
            # print(f"Button [released]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.StopAxis('x')
        else:
            ApplicationHandler.StopAxis('x')

    return render_template(HTML)

@app.route('/moveAxisY', methods=['POST', "GET"])
def moveAxisZUpward():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        buttonState = request.form['javascript_data']
        
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        if (buttonState == 'pressForward'):
            # print(f"Move forward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('y', ApplicationHandler.jogDirections.Forward)
        elif (buttonState == 'pressBackword'):
            # print(f"Move backward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('y', ApplicationHandler.jogDirections.Backward)
        elif (buttonState == 'releaseForward' or buttonState == 'releaseBackword'):
            # print(f"Button [released]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.StopAxis('y')
        else:
            ApplicationHandler.StopAxis('y')

    return render_template(HTML)

@app.route('/moveAxisZ', methods=['POST', "GET"])
def moveAxisYUpward():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        buttonState = request.form['javascript_data']
        
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        if (buttonState == 'pressForward'):
            # print(f"Move forward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('z', ApplicationHandler.jogDirections.Forward)
        elif (buttonState == 'pressBackword'):
            # print(f"Move backward [pressed]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.JogAxis('z', ApplicationHandler.jogDirections.Backward)
        elif (buttonState == 'releaseForward' or buttonState == 'releaseBackword'):
            # print(f"Button [released]: {datetime.now().strftime('%I:%M:%S.%f')}")
            ApplicationHandler.StopAxis('z')
        else:
            ApplicationHandler.StopAxis('z')

    return render_template(HTML)

@app.route('/setStartingPoint', methods=['POST', "GET"])
def setStartingPoint():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        ApplicationHandler.SetCurrentPositionAsStartingPoint()

    return render_template(HTML)

@app.route('/gotToStartingPoint', methods=['POST', "GET"])
def GotToStartingPoint():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "POST" and len(request.form) > 0:
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True
        if ApplicationHandler.SUCCESSFUL_HOMING == True:
            ApplicationHandler.MoveAxisesToStartingPoint()
        else:
            print("Please Home the System!!!")
            flash("Please Home the System!!!")

    return render_template(HTML)

@app.route('/getpythondata',  methods=['GET'])
def GetPythonData():
    global ApplicationHandler
    global ApplicationHandlerInitialized
    global HTML

    if request.method == "GET":
        if ApplicationHandlerInitialized == False:
            ApplicationHandler = LiquidHandlerHal()
            ApplicationHandlerInitialized = True

        axisesCoordinates = ApplicationHandler.GetAxisesCoordinates()
        jsonData = json.dumps(axisesCoordinates)

    # return render_template(
    #     HTML,
    #     Data = jsonData
    #     )

    return jsonData
    
if __name__ == '__main__':
    app.run(debug = True, host='0.0.0.0')
    