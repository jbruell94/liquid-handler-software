import json


"""Parses json structured data based on Opentrons protocol.
Provides interfaces for getting all the available data under the tag 'commands'
in the protocol"""
class CommandParser():
    def __init__(self, jsonData: dict):
        self._allCommands = jsonData["commands"]

    def getType(self, commandNum: int):
        
        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getType()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        return command["command"]

    def getParams(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getParams()): Index [{commandNum}] out of range")
            return None
        
        command = self._allCommands[commandNum]
        return command["params"]

    def getFlowRate(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getFlowRate()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "flowRate" in params:
            return params["flowRate"]
        else:
            print("No flowRate in command: {0} : {1}".format(commandNum, command["command"]))
            return None
            
    def getLabware(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getLabware()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "labware" in params:
            return params["labware"]
        else:
            print("No labware in command: {0} : {1}".format(commandNum, command["command"]))
            return None

    def getOffsetFromBottom(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getOffsetFromBottom()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "offsetFromBottomMm" in params:
            return params["offsetFromBottomMm"]
        else:
            print("No offsetFromBottomMm in command: {0} : {1}".format(commandNum, command["command"]))
            return None

    def getPipetteName(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getOffsetFromBottom()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "pipette" in params:
            return params["pipette"]
        else:
            print("No pipette in command: {0} : {1}".format(commandNum, command["command"]))
            return None

    def getVolume(self, commandNum: int):

        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getVolume()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "volume" in params:
            return params["volume"]
        else:
            print("No volume in command: {0} : {1}".format(commandNum, command["command"]))
            return None


    def getWell(self, commandNum: int):
        if 0 > commandNum >= len(self._allCommands):
            print(f"Error (getWell()): Index [{commandNum}] out of range")
            return None

        command = self._allCommands[commandNum]
        params = command["params"]
        if "well" in params:
            return params["well"]
        else:
            print("No well in command: {0} : {1}".format(commandNum, command["command"]))
            return None

"""Parses json structured data based on Opentrons protocol.
Provides interfaces for getting all the available data under the tags 'labware' and 'labwareDefinitions'
in the protocol"""
class LabwareParser():
    def __init__(self, jsonData: dict):
        
        self._allLabwares           = jsonData["labware"]
        self._labwareDefinitions    = jsonData["labwareDefinitions"]

    def getDefinitionId(self, wabId: str):
        lab = self._allLabwares[wabId]

        if 'definitionId' in lab:
            return lab['definitionId']
        else:
            print(f"Warning: No 'definitionId' for labware {wabId}")
            return None

    def getDisplayName(self, wabId: str):
        lab = self._allLabwares[wabId]

        if 'displayName' in lab:
            return lab['displayName']
        else:
            print(f"Warning: No 'displayName' for labware {wabId}")
            return None

    def getSlot(self, wabId: str):
        lab = self._allLabwares[wabId]

        if 'slot' in lab:
            return lab['slot']
        else:
            print(f"Warning: No 'slot' for labware {wabId}")
            return None

    def getLabwareDefinition(self, wabId: str):
        lab = self._allLabwares[wabId]
        
        if 'definitionId' in lab:
            defId = lab['definitionId']
            return self._labwareDefinitions[defId]
        else:
            print(f"Warning: No 'cornerOffsetFromSlot' for labware {wabId}")
            return None

    def getCornerOffsetFromSlot(self, wabId: str, axis: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        

        if 'cornerOffsetFromSlot' in labDef:
            offset = labDef["cornerOffsetFromSlot"]
            if axis != 'x' and axis != 'y' and axis != 'z':
                return -1
            else:
                return offset[axis]
        else:
            print(f"Warning: No 'cornerOffsetFromSlot' for labware {wabId}")
            return None
    
    def getDimensionsXYZ(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        
        if 'dimensions' in labDef:
            dimensions = labDef["dimensions"]
            return dimensions['xDimension'], dimensions['yDimension'], dimensions['zDimension']
        else:
            print(f"Warning: No 'dimensions' for labware {wabId}")
            return None

    # def getWellBottomShape(self, wabId: str):
    #     lab = self._allLabwares[wabId]
    #     defId = lab['definitionId']
    #     labDef = self._labwareDefinitions[defId]
    #     grp = labDef['groups']
    #     grpMetadata = grp[0]
    #     metadata = grpMetadata["metadata"]
    #     wellBottomShape = metadata["wellBottomShape"]
    #     return wellBottomShape

    def getDisplayVolumeUnits(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        metadata = labDef['metadata']

        if 'displayVolumeUnits' in metadata:
            return metadata['displayVolumeUnits']
        else:
            print(f"Warning: No 'displayVolumeUnits' for labware {wabId}")
            return None

    def getWells(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]

        if 'groups' in labDef:
            groups = labDef['groups']
            wells = []

            for grp in groups:
                wells.append(grp["wells"])
            return wells
        else:
            print(f"Warning: No 'groups' for labware {wabId}")
            return None

    def getWellsOrdering(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        
        if 'ordering' in labDef:
            return labDef['ordering']
        else:
            print(f"Warning: No 'ordering' for labware {wabId}")
            return None

    def getFormat(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'format' in parameters:
            return parameters['format']
        else:
            print(f"Warning: No 'format' for labware {wabId}")
            return None


    def getIsMagneticModuleCompatible(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'isMagneticModuleCompatible' in parameters:
            return parameters['isMagneticModuleCompatible']
        else:
            print(f"Warning: No 'isMagneticModuleCompatible' for labware {wabId}")
            return None

    def getIsTiprack(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'isTiprack' in parameters:
            return parameters['isTiprack']
        else:
            print(f"Warning: No 'isTiprack' for labware {wabId}")
            return None

    def getLoadName(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'loadName' in parameters:
            return parameters['loadName']
        else:
            print(f"Warning: No 'loadName' for labware {wabId}")
            return None

    def getTipLength(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'tipLength' in parameters:
            return parameters['tipLength']
        else:
            print(f"Warning: No 'tipLength' for labware {wabId}")
            return None


    def getTipOverlap(self, wabId: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        parameters = labDef['parameters']

        if 'tipOverlap' in parameters:
            return parameters['tipOverlap']
        else:
            print(f"Warning: No 'tipOverlap' for labware {wabId}")
            return None

    def getWellSettings(self, wabId: str, wellName: str):
        lab = self._allLabwares[wabId]
        defId = lab['definitionId']
        labDef = self._labwareDefinitions[defId]
        wells = labDef['wells']
        wellSettings = wells[wellName]

        return wellSettings

class Pipette():
    def __init__(self, jsonData: dict):
        
        self._pipette = jsonData["pipettes"]

"""Parses json structured data based on Opentrons protocol.
Provides interfaces for getting all the available data provided with dictionary of 'wellSettings'"""
class WellParser():
    def __init__(self, wellSettings):

        self._wellSettings = wellSettings

    def getDepth(self):
        if 'depth' in self._wellSettings:
            return self._wellSettings['depth']
        else:
            print("Warning: No 'depth' available for current well")
            return None

    def getDiameter(self):
        if 'diameter' in self._wellSettings:
            return self._wellSettings['diameter']
            print("Warning: No 'diameter' available for current well")
        else:
            return None

    def getShape(self):
        if 'shape' in self._wellSettings:
            return self._wellSettings['shape']
        else:
            print("Warning: No 'shape' available for current well")
            return None

    def getTotalLiquidVolume(self):
        if 'totalLiquidVolume' in self._wellSettings:
            return self._wellSettings['totalLiquidVolume']
        else:
            print("Warning: No 'totalLiquidVolume' available for current well")
            return None

    def getPositionX(self):
        if 'x' in self._wellSettings:
            return self._wellSettings['x']
            print("Warning: No 'x' available for current well")
        else:
            return None

    def getPositionY(self):
        if 'y' in self._wellSettings:
            return self._wellSettings['y']
            print("Warning: No 'y' available for current well")
        else:
            return None

    def getPositionZ(self):
        if 'z' in self._wellSettings:
            return self._wellSettings['z']
        else:
            print("Warning: No 'z' available for current well")
            return None