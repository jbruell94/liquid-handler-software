from pymodbus.client.sync import ModbusTcpClient as TcpClient
from pymodbus.client.sync import ModbusSerialClient as SerialClient
from enum import Enum
import serial
import time

class _ModbusCommands:    
#input registers
    AirlockPressureTLow=65
    AirlockPressureTHigh=66
    AirlockPressureWLow=67
    AirlockPressureWHigh=68
    TipPressureLow=69
    TipPressureHigh=70
    PipetteStatus=71
    FirmwareVer=72
    FirmwareSubVer=73
    FirmwareBuild=74
    LogDataEntries=75
    LogDataEntry1=76
    LogDataEntry2=77
    LogDataEntry3=78
    LogDataEntry4=79
    TipDetectSelect=80
    BootloaderUpdateStatus=81
    AirlockOpenCycleCounterLow=164
    AirlockOpenCycleCounterHigh=165
    DispenseOpenCycleCounterLow=166
    DispenseOpenCycleCouonterHigh=167
    TipEjectionCounterLow=168
    TipEjectionCounterHigh=169
    LIfeCounterLow=170
    LifeCounterHigh=171
    ErrorCodesStatus=172
    SavedFrameBitmask=180
    SavedFrameFirstChunk=181

#holding registers
    AirlockPressureOverride=4096
    AirlockVaccumOverride=4097
    DispensePort1Override=4098
    DispensePort2Override=4099
    PressureAirlockSPIn=4100
    PressureAirlockSPOut=4101
    ManualAbort=4102
    TimedOpenPressValve=4109
    TimedOpenVacuumValve=4110
    TimedOpenDispenseValve=4111
    EjectTipCommand=4112
    StoreEepromCommand=4113
    ApplicationModesSelect=4114
    WebServerResetCommand=4115
    IpAddressLow=4196
    IpAddressHigh=4197
    GatewayLow=4198
    GatewayHigh=4199
    NetMaskLow=4200
    NetMaskHigh=4201
    MacAddress1=4202
    MacAddress2=4203
    MacAddress3=4204
    TcpPort=4205
    ModbusSlaveAddress=4206
    KpDispenseUpLow=4207
    KpDispenseUpHigh=4208
    KiDispenseUpLow=4209
    KiDispenseUpHigh=4210
    KpDispenseDownLow=4211
    KpDispenseDownHigh=4212
    KiDispenseDownLow=4213
    KiDispenseDownHigh=4214
    KpAirlockLow=4215
    KpAirlockHigh=4216
    KiAirlockLow=4217
    KiAirlockHigh=4218
    DispenseFilterUpLow=4223
    DispenseFilterUpHigh=4224
    DispenseFilterDownLow=4225
    DispenseFilterDownHigh=4226
    AirlockFilterLow=4227
    AirlockFilterHigh=4228    
    DispenseOpenThrsh1=4231
    DispenseOpenThrsh2=4232
    AirlockPressureOpenThrsh=4233
    AirlockVacuumOpenThrsh=4234

#multiple holding registers
    IpAddress=4196
    Gateway=4198
    NetMask=4200
    MacAddress=4202
    KpDispenseUp=4207
    KiDispenseUp=4209
    KpDispenseDown=4211
    KiDispenseDown=4213
    KpAirlock=4215
    KiAirlock=4217
    DispenseFilterUp=4223
    DispenseFilterDown=4225
    AirlockFilter=4227
    ProductionDate=4296
    PartNum=4298
    SerNum=4300



class DHOP:
    def __init__(self):
        self.dhopConfig = object
        self.sensData = {"AirlockPressureTight" : 0.0,
                        "AirlockPressureWide" : 0.0,
                        "TipPressure" : 0.0}

    def init(self, dhopConfig):
        self.dhopConfig = dhopConfig
        try:
            self.client = TcpClient(host=dhopConfig["ip"], port=dhopConfig['tcpPort'])            
            self.client.connect()
        except:
            print("Incorrect Modbus configuration")


    def readData(self, register, sign=True):
        data = 0
        try:
            data = self.client.read_input_registers(register, 1, unit=self.dhopConfig['modbusSlave'])
        except Exception as e:
            print(f"Error while reading: {e}")

        return data.registers[0]

    def writeData(self, register, val, sign=True):
        status = 1        
        if val < 0:
            val = val + 2**16
        try:
            self.client.write_register(register, value=val, unit=self.dhopConfig['modbusSlave'])
            #check if pipette still busy (Status word)
            while (status & 32) == 1:
                status = self.client.read_input_registers(_ModbusCommands.PipetteStatus, 1, unit=self.dhopConfig['modbusSlave']) 
        except Exception as e:
            print(f"error while writing: {e}")
 
    def aspirate(self, microL:int):

        print("aspirating")
        #aspiration is done statically with experimanetal values
        self.writeData(_ModbusCommands.PressureAirlockSPIn, -80)
        time.sleep(1)
        self.writeData(_ModbusCommands.PressureAirlockSPOut, -30)
        time.sleep(1)
        status = self.readData(_ModbusCommands.PipetteStatus)
        if status == 64:
            raise Exception("Aspirating Aborted")


    def dispense(self, microL:int):
        print("dispensing")
        self.writeData(_ModbusCommands.PressureAirlockSPIn, 80)
        time.sleep(1)
        self.writeData(_ModbusCommands.PressureAirlockSPOut, 30)
        time.sleep(1)
        status = self.readData(_ModbusCommands.PipetteStatus)
        if status == 64:
            raise Exception("Dispensing Aborted")

    def mix(self):
        print("mixing")
        for i in range(3):
            self.writeData(_ModbusCommands.PressureAirlockSPIn, -80)
            time.sleep(1)
            self.writeData(_ModbusCommands.PressureAirlockSPOut, -30)
            time.sleep(1)
            status = self.readData(_ModbusCommands.PipetteStatus)

            if status == 64:
                raise Exception("Aspirating Aborted")

            self.writeData(_ModbusCommands.PressureAirlockSPIn, 80)
            time.sleep(1)
            self.writeData(_ModbusCommands.PressureAirlockSPOut, 30)
            time.sleep(1)
            status = self.readData(_ModbusCommands.PipetteStatus)

            if status == 64:
                raise Exception("Dispensing Aborted")

    def readSensData(self):
        self.sensData['AirlockPressureTight'] = float((self.readData(_ModbusCommands.AirlockPressureTHigh) << 16) + self.readData(_ModbusCommands.AirlockPressureTLow))
        print("Sens dtaa:", self.sensData['AirlockPressureTight'])

    def ejectTip(self):
        print("eject tip")
        self.writeData(_ModbusCommands.EjectTipCommand, 1)


def main():
    dhopConf = {
        "ip" : "192.168.10.102",
        "tcpPort" : 502,
        "modbusSlave" : 17
    }    

    dhop = DHOP()
    dhop.init(dhopConf)
    dhop.aspirate(20)
    #dhop.dispense(20)
    dhop.readSensData()
    print("done")
    while 1:
        pass

if __name__ == '__main__':
    main()

