from abc import ABC, abstractmethod

class digitalModules(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def readAllDigital(self):
        pass

    @abstractmethod
    def writeAllDigital(self, digitalOut):
        pass
    
    @abstractmethod
    def readDigitalChannel(self, channel):
        pass
    
    @abstractmethod
    def writeDigitalChannel(self, digitalOut, channel):
        pass



