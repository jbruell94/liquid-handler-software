from typing import Dict, Any, List
from pathlib import Path
import json
from json.decoder import JSONDecodeError


from config.data_types import AxisConfig
from config import AXES_CONFIG_FILE, CONFIG_LOG
class AxisEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, AxisConfig):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

DEF_AXES_NAMES = ['x', 'y', 'z']
DEF_AXES_ID = 1 #must edit for each axis in config file afterwards
DEFAULT_REPLY_ID = 0
DEF_ENCODER_AVAIL = False
DEF_RESOLUTION = 8
DEF_RUN_CURRENT = 128
DEF_STANDBY_CURRENT = 8
DEF_MOTOR_STEPS_PER_REV = 200
DEF_PWM_AUTO_SCALE = 0
DEF_HOME_VELOCITY = 90800 #edit the sign for correct direction
DEF_SAFETY_OFFSET = 20
DEF_MAX_DEVIATION = 0
DEF_STALLGUARD_TR = 1 #edit in generated config, very sensitive
DEF_STOP_ON_STALL_VEL = 60000 #edit in config, very sensitive for homing
DEF_STALLGUARD_FILTER = 0
DEF_FULLSTEP_MM = 0 #define as per motor specification
DEF_ADDITIONAL_OFFSET = 0
DEF_TRAVEL_HIGHT_OFF = 0
DEF_STARTING_POINT = 0 #edit for each axis
DEF_JOG_VELOCITY = 50000
DEF_MAX_MOVEMENT = 100 #edit for each axis
DEF_CONTROLLER_TYPE = "TMCM_6214"
DEF_REPLY_ID = 0

DEF_RAMP_SETTINGS = {
    "startVelocity": 1,
    "accelerationA1": 240000,
    "velocityV1": 230400,
    "maxAccelerationA2": 500000,
    "maxVelocity": 460800,
    "maxDecelerationD2": 460800,
    "decelerationD1": 240000,
    "stopVelocity": 1,
    "rampWaitTime": 1
}



DEF_EXT_CONTROL_SETT = {
    "controllerType": DEF_CONTROLLER_TYPE,
    "id": DEF_AXES_ID,
    "replyID": DEF_REPLY_ID,
    "encoderAvailable": False,
    "resolution": DEF_RESOLUTION,
    "runCurrent": DEF_RUN_CURRENT,
    "standbyCurrent": DEF_STANDBY_CURRENT,
    "PWMAutoScale": DEF_PWM_AUTO_SCALE,
    "homeVelocity": DEF_HOME_VELOCITY,
    "StallGuard2Treshold": DEF_STALLGUARD_TR,
    "StopOnStallVelocity": DEF_STOP_ON_STALL_VEL,
    "Stallguard2Filter": DEF_STALLGUARD_FILTER
}

def _build_axes_config(axesJson: Dict[str, Any]) -> List[AxisConfig]:
    axis_config: Dict[str, Any]

    axis_config = {}
    axes = []

    for ax in (list(map(lambda x: x['name'], axesJson)) if axesJson else DEF_AXES_NAMES):        
        
        axis_config = list(filter(lambda a: a['name'] == ax, axesJson)).pop() if axesJson else {}

        axis = AxisConfig(
            name=axis_config.get('name', ax),
            safetyOffset=axis_config.get('safetyOffset', DEF_SAFETY_OFFSET),
            additionalOffset=axis_config.get('additionalOffset', DEF_ADDITIONAL_OFFSET),
            maxDeviation=axis_config.get('maxDeviation', DEF_MAX_DEVIATION),
            fullStep_mm=axis_config.get('fullStep_mm', DEF_FULLSTEP_MM),
            jogVelocityForward=axis_config.get('jogVelocityForward', DEF_JOG_VELOCITY),
            jogVelocityBackward=axis_config.get('jogVelocityBackward', (-1)*DEF_JOG_VELOCITY),
            maxMovementmm=axis_config.get('maxMovementmm', DEF_MAX_MOVEMENT),
            startingPoint=axis_config.get('startingPoint', DEF_STARTING_POINT),
            travelHightOffsetMm=axis_config.get('travelHightOffsetMm', DEF_TRAVEL_HIGHT_OFF),
            motorStepsPerRev=axis_config.get('motorStepsPerRev', DEF_MOTOR_STEPS_PER_REV),
            controllerSettings=axis_config.get('controllerSettings', DEF_EXT_CONTROL_SETT),
            rampSettings=axis_config.get('rampSettings', DEF_RAMP_SETTINGS)
        )
        axes.append(axis)
    
    return axes

def _write_config(configs: List, config_path: Path, encoder):
    try:
        with open(config_path, 'w') as file:
            json.dump(configs, file, cls=encoder)
    except PermissionError:
        CONFIG_LOG.error("Could not write to file. Forbidden!")
        return

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        CONFIG_LOG.warning(f"Could not load file{config_path}, Loading Default settings")
    except JSONDecodeError:
        CONFIG_LOG.warning(f'decoder error {config_path}, Loading Default settings')

    return config

def get_config() -> List[AxisConfig]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(AXES_CONFIG_FILE)
    try:
        config = _build_axes_config(json_config)
    except:
        CONFIG_LOG.error("Failed to build motion config!")
        return None
    _write_config(config, AXES_CONFIG_FILE, AxisEncoder)
    return config

def set_config(axes_config: List[AxisConfig]):
    """Writes the desired configuration to the config file
    """
    _write_config(axes_config, AXES_CONFIG_FILE, AxisEncoder)

if __name__ == "__main__":
    
    json_config = _load_config(AXES_CONFIG_FILE)
    config = _build_axes_config(json_config)
    _write_config(config, AXES_CONFIG_FILE, AxisEncoder)
