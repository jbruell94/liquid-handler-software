from dataclasses import dataclass, asdict
from typing import List, NamedTuple, TypedDict, Dict, Any

class extControlSettings(TypedDict):
    controllerType: str
    id: int
    replyID: int
    encoderAvailable: bool
    resolution: int
    runCurrent: int
    standbyCurrent: int
    PWMAutoScale: int
    homeVelocity: int
    StallGuard2Treshold: int
    StopOnStallVelocity: int
    Stallguard2Filter: int

class RampSettings(NamedTuple):
    startVelocity: int
    accelerationA1: int
    velocityV1: int
    maxAccelerationA2: int
    maxVelocity: int
    maxDecelerationD2: int
    decelerationD1: int
    stopVelocity: int
    rampWaitTime: int

@dataclass
class AxisConfig():
    name: str
    safetyOffset: int
    maxDeviation: int
    fullStep_mm: int
    additionalOffset: int
    travelHightOffsetMm: int
    startingPoint: int
    jogVelocityForward: int
    jogVelocityBackward: int
    maxMovementmm: int
    motorStepsPerRev: int
    controllerSettings: extControlSettings
    rampSettings: RampSettings


@dataclass
class Pgva():
    interface       : str
    baudrate        : int
    comPort         : str
    ip              : str
    tcpPort         : int
    modbusSlave     : int

@dataclass
class Vaem():
    ip              : str
    port            : int
    slave_id        : int

@dataclass
class AnalogModule():
    ip              : str
    port            : int
    module          : str
    modbusSlave     : int

@dataclass
class UtrasonicSensor():
    sensitivityLevel: str
    teachingDevice: Dict[str, Any]
    readingDevice: Dict[str, Any]

@dataclass
class PressureControl():
    readingDevice: str
    channel : int
    maxVoltage : int
    maxPressure: int
    dacResolution: int

@dataclass
class CalibrationData():
    point1: List[int]
    point2: List[int]
    point3: List[int]
    yaw: float
    pitch: float
    roll: float