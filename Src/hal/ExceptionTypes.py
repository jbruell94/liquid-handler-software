
class AxesNotSetupError(Exception):
    def __init__(self):
        super().__init__('Not all axes are setup correctly. Please check connections and reboot!')
class PgvaNotAvailabe(Exception):
    def __init__(self):
        super().__init__('No PGVA availabe. Please connect at least one PGVA!')

class VaemNotAvailable(Exception):
    def __init__(self):
        super().__init__('No VAEM Available. Please connect at least one VAEM!')

class AdamNotAvailableError(Exception):
    def __init__(self):
        super().__init__('One or all IO is not able to connect!')

class SensorNotAvailableError(Exception):
    def __init__(self):
        super().__init__('Not able to initialize ultrasonic sensors')

class PressureGenerationNotAvailableError(Exception):
    def __init__(self):
        super().__init__('Not able to initialize pressure generator units sensors')

