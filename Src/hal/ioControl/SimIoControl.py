import asyncio
from asyncio.events import get_event_loop
from typing import Any, List

#TODO define custon exceptions
from hal.ExceptionTypes import AdamNotAvailableError, SensorNotAvailableError, PressureGenerationNotAvailableError
from hal.ioControl import IO_LOG
from config import io_config
from random import random
class SimIOControl():
    """HAL class that controls input output devices,
    such as digital inputs/outputs and analog inputs/outputs
    """
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            IO_LOG.info(f"Initilized IO Simulation Control class. One instance allowed")
            cls._instance = super(SimIOControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self._adamModules : List[Any] = None
        self._sensorModules : List[Any] = None
        self._pressModules = None
        self._pressureSet : List[int] = []
        self._digOutSet : List[bool] = [False for _ in range(8)]
        try:
            self._adamConfig = io_config.get_adam_config()
            self._adamModules = list(map(lambda x: x, self._adamConfig))
        except ConnectionError:
            IO_LOG('Could not connect to one or all Adams. Please check the configuration or cabling!')
            raise AdamNotAvailableError

        if self._adamModules is not None:
            self._sensorConfig = io_config.get_ultrasonic_config()
            self._sensorModules = list(map(lambda x: x, self._sensorConfig))
        else:
            IO_LOG.error('Could not initialize sensors because of connection problems with ADAM devices!')
            raise SensorNotAvailableError
            
        if self._adamModules is not None:
            self._pressConfig = io_config.get_pressure_config()
            for _ in self._pressConfig:
                self._pressureSet.append(0)
        else:
            IO_LOG.error('Could not initialize the pressure generation units. No Adam connected!')
            raise PressureGenerationNotAvailableError


    async def get_ultrasonic(self, sensor_id: int):
        """
        Get the sensor measurment by id. 
        Id is the channel of the Adam module. ID is a number fomr config file
        """    
        if sensor_id in range(0, len(self._sensorModules)):
            for _ in range(10):
                await asyncio.sleep(0.1)
            return random() * 100
        else:
            raise ValueError("No such id configured")

    async def set_pressure(self, pressure: float, id: int):
        """
        Set the pressure inside the pressure controller chosen.
        Here the conversion is made from mBar to Volts
        max voltage -> check config file
        max pressure -> check config file
        id -> id of the Vaeb(integer), please check the config for valid indeces
        """
        if id in range(0, len(self._pressConfig)):
            if pressure in range(self._pressConfig[id].maxPressure):
                analogSet = pressure * (self._pressConfig[id].dacResolution/self._pressConfig[id].maxPressure)
                self._pressureSet[id] = analogSet
            else:
                IO_LOG.warn(f'Tried to set pressure outside of range {pressure}')
                raise ValueError(f'Pressure value out of range {pressure}')
        else:
           raise IndexError("Invalid id")

    def get_pressure(self, id: int):
        """
        Get the pressure inside the pressure controller chosen.
        Here the conversion is made from Volts to mBar
        max voltage -> check config file
        max pressure -> check config file
        id -> id of the Vaeb(integer), check config for supported indeces
        """
        if id in range(0, len(self._pressConfig)):
            analogOut = self._pressureSet[id]
            pressure = analogOut * (self._pressConfig[id].maxPressure/self._pressConfig[id].dacResolution)
            return pressure
        else:
            IndexError("Invalid pressure gen index")


    async def set_digital_out(self, channel, state):
        """
        Set the required digital channels.
        channel -> bit mask for desired channels
        state -> state of the required channels
        e.g. channel = 3, state = 1 => channel 1 is set to 1 and channel 2 is set to 0
        """
        for i in range(0, 8):
            #print(f"channel : {bin(channel)}, state : {bin(state)}")
            if channel & 0x01:
                if state & 0x01:
                    self._digOutSet[i] = True
                else:
                    self._digOutSet[i] = False
            channel = channel >> 1
            state = state >> 1
            #print(f"channel : {bin(channel)}, state : {bin(state)}, i : {i}")


    async def get_digital_out(self):
        """
        Get the state of the digital outputs
        The result is in bit form
        e.g. res = 3 => channels 1 and 2 are ON
        """
        digOuts :int = 0
        diglist = self._digOutSet
        for i in range(0, len(diglist)):
            #print(tmp[i])
            if diglist[i]:
                digOuts = (digOuts << 1) | 0x01
        return digOuts

"""
    def LedSetColor(self, opperation:str):

        try:
            if opperation == 'Idle':                
                self.LedHandler.ledStripSet(LedControl.COLORS.FestoColor)
            elif opperation == "Running":
                self.LedHandler.ledStripSet(LedControl.COLORS.RandomColor)
            elif opperation == "Error":
                self.LedHandler.ledStripSet(LedControl.COLORS.ErrorColor)
            else:
                self.LedHandler.ledStripSet(LedControl.COLORS.WhiteColor)
        except Exception as e:
            print(f"Setting Led strip failed {e}")
"""


if __name__ == "__main__":
    async def func():
        io = SimIOControl()
        await io.set_digital_out(255, 255)
        print(bin(await io.get_digital_out()))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())


