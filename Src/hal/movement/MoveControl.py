import asyncio
from logging import Logger
from os import stat
from typing import Dict, Any, List
from copy import deepcopy

from hal.movement import LOGGER
from hal.statusManager.MoveStateManager import MovementStateManager, StateRequestsTypes
from hal.movement.axisFactory import AxisFactory
import config.motion_config as mc
from hal.statusManager.StatusDataTypes import AxisStatus, AxisStatusTypes
from hal.ExceptionTypes import AxesNotSetupError
class MovementControl():
    """This module contains the basic movement 
    functions availabe for this gantry
    The module uses the asincio package in 
    order not to block the execution while moving
    This must be a singleton class object

    @param: loop- the async event to be used in this module
    """
    _instance = None
    _controller_connected = False
    _axes_status: List[AxisStatus] = []
    _stopAxisReq: bool = False
    _move_req: List[MovementStateManager] = []
    def __new__(cls):
        if cls._instance is None:
            LOGGER.info("creating Movement control class. Using only this instance from now on!")
            cls._instance = super(MovementControl, cls).__new__(cls)
        return cls._instance
    
    def __init__(self) -> None:
        self._axisSetup = False
        self._stopAxisReq = False
        self.axesConfig = mc.get_config()
        LOGGER.info(f'Loaded the following configuration {self.axesConfig}')
        self.tmcmAxis = {}

    @property
    def axes_status(self):
        return self._axes_status

    async def build_axes(self):       
        try: 
            for config in self.axesConfig:
                self.tmcmAxis[config.name] = AxisFactory.createAxis(controllerType=config.controllerSettings.get('controllerType', {}), 
                                                                    id=config.controllerSettings.get('id', {}), 
                                                                    hostID=config.controllerSettings.get('replyID', {}))
                #add axes while still in Init mode
                self._axes_status.append(AxisStatus(config.name, status=AxisStatusTypes.IDLE))
                #add pause manager for each axis
                self._move_req.append(MovementStateManager(config.name, StateRequestsTypes.IDLE))

                LOGGER.info(f'{self.tmcmAxis[config.name]},  {config.name}, {config}')
                await self._configure_axis(self.tmcmAxis[config.name], config.name, config)  
            self._controller_connected = True          
        except ValueError:
            LOGGER.error(f'error in configuration')  
        except Exception as e:
            LOGGER.error(f'Axes cannot be initialized: {e}')
            LOGGER.info('Please check connection, configuration, then reboot and try again!')

    async def _configure_axis(self, tmcm, name: str, axisConfig):
        if tmcm: 
            tmcm.setStallGuard2Treshold(axisConfig.controllerSettings.get("StallGuard2Treshold", 0))
            tmcm.setStopOnStallVelocity(axisConfig.controllerSettings.get("StopOnStallVelocity", 0))
            tmcm.setStallguard2Filter(axisConfig.controllerSettings.get("Stallguard2Filter", 0))   
            tmcm.setMaxVelocity(int(axisConfig.rampSettings.get("maxVelocity", 0)))
            tmcm.setMaxAcceleration(axisConfig.rampSettings.get("maxAccelerationA2", 0))
            tmcm.setMotorMicroStepResolution(axisConfig.controllerSettings.get("resolution", 0))
            # TODO adapt regarding resolution
            # self.axises[axisKey].setSmartEnergyThresholdSpeed(76800)
            # self.axises[axisKey].setPWMThresholdSpeed(76800)
            # self.axises[axisKey].setPWMGrad(15)
            # self.axises[axisKey].setPWMAmplitude(65)
            # self.axises[axisKey].setPWMAutoscale(self.axisConfig[axisKey].PWMAutoScale)
            tmcm.setMotorMaxRunCurrent(axisConfig.controllerSettings.get("runCurrent", 0))
            tmcm.setMotorStandbyCurrent(axisConfig.controllerSettings.get("standbyCurrent", 0))
            # Setup RAMP settings
            tmcm.setStartVelocity(axisConfig.rampSettings.get("startVelocity", 0))
            tmcm.setA1(axisConfig.rampSettings.get("accelerationA1", 0))
            tmcm.setV1(axisConfig.rampSettings.get("velocityV1", 0))
            tmcm.setMaxAcceleration(axisConfig.rampSettings.get("maxAccelerationA2", 0))
            tmcm.setMaxVelocity(axisConfig.rampSettings.get("maxVelocity", 0))
            tmcm.setStartVelocity(axisConfig.rampSettings.get("startVelocity", 0))
            tmcm.setMaxDeceleration(axisConfig.rampSettings.get("maxDecelerationD2", 0))
            tmcm.setD1(axisConfig.rampSettings.get("decelerationD1", 0))
            tmcm.setStopVelocity(axisConfig.rampSettings.get("stopVelocity", 0))
            tmcm.setRampWaitTime(axisConfig.rampSettings.get("rampWaitTime", 0))
            tmcm.setMaxEncoderDeviation(axisConfig.maxDeviation)
            tmcm.stop()
        else:
            raise ValueError     


    async def home_axis(self, axis: str):

        """ 
        Homes 1 axis with id
        It uses the stallguard configuration to stop the motors at end point.         
        When the axis is home, it sets the actual possition of the axis to zero
        As a finel step it moves the axis at 20mm from 0

        @param axis:str -> this parameter is dependent on the configuration parameter 'name'
        """
        try:
            if (self._controller_connected == False):
                LOGGER.error('Axis is nost setup. Please try again after successful connection')
                raise AxesNotSetupError
            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
            status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()

            # Setup  one axis at a time
            if ax:
                
                #clear axis errors
                LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')

                self.tmcmAxis[axis].rotate(ax.controllerSettings["homeVelocity"])
                status.status = AxisStatusTypes.HOMING
                LOGGER.info(f'Started Homing of axis {axis}')
                while (self.tmcmAxis[axis].getActualVelocity() != 0 and self.tmcmAxis[axis].getErrorFlags() == 0):
                    await asyncio.sleep(0.1)
                    
                self.tmcmAxis[axis].setActualPosition(0)
                await asyncio.sleep(1)

                self.tmcmAxis[axis].stop()

                status.status = AxisStatusTypes.IDLE
                status.homed = True

                await self.move_axis(axis, ax.safetyOffset)

                LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')
                LOGGER.info(f'Load Values: {self.tmcmAxis[axis].getLoadValue()}')
                
                if ax.controllerSettings["encoderAvailable"]:
                    self.tmcmAxis[axis].setEncoderPosition(0)


        except Exception as e:
            LOGGER.error(f'Homing of axis {axis} FAILED!, Please try homing again')
            LOGGER.info(f'Exception :{e}')


    async def jog_axis(self, axis: str, forward: bool):
        """Starts the movement of the given 'axis' in forward or backward direction with velocity set in the configuration
        Velocity for forward and backward could be set with different values
        
        @param: axis - one of the names from the configuration file
        @param: forward - true for moving in the forward direction with positive velocity
        """
        try:
            if (self._controller_connected == False):
                raise Exception(f"Warning: Action 'JogAxis()' refused! Axis {axis} not setup correctly!")
            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
            status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()
            pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
            if ax and pause_req.state != StateRequestsTypes.STOP:
                status.status = AxisStatusTypes.MOVING
                if forward:
                    self.tmcmAxis[axis].rotate(ax.jogVelocityForward)
                    while pause_req.state != StateRequestsTypes.STOP:
                        if (self.tmcmAxis[axis].getErrorFlags() == 1):
                            LOGGER.warn(f'Hit the end of the gantry! Stopping Axis')
                            self.stop_axis(axis)
                        await asyncio.sleep(0.1)
                else:
                    self.tmcmAxis[axis].rotate(ax.jogVelocityBackward)
                    while pause_req.state != StateRequestsTypes.STOP:
                        if (self.tmcmAxis[axis].getErrorFlags() == 1):
                            LOGGER.warn(f'Hit the end of the gantry! Stopping Axis')
                            self.stop_axis(axis)
                        await asyncio.sleep(0.1)

                status.status = AxisStatusTypes.IDLE
        except Exception as e:
            LOGGER.error(f"Something went wrong (JogAxis({axis})): {e}")

    async def move_axis(self, axis:str, distance_mm: float):
        """
        Move given 'axis' relatively by given 'distance_mm'.
        automatic conversion is done from mm to steps
        @param name: from configuration file
        @param distance_mm: distance to move in mm. Will not move if the distance is larger that 
            that max movement
        """
        try:
            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
            pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
            if self._controller_connected == False:
                LOGGER.error(f"Axis not setup. Cannot move {axis}")
                raise AxesNotSetupError

            #TODO This idle will check for all three axes together
            status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()
            if status.status == AxisStatusTypes.IDLE and status.homed:
                if((ax.maxMovementmm < distance_mm) or (distance_mm < 0)):
                    LOGGER.error(f'Requested move is beyond the capabilities of the axis {axis}')
                    return
                # Due to always positive value for input distance, multiply by -1 depending on home velocity sign
                if ax.controllerSettings["homeVelocity"] > 0:
                    distance_mm = distance_mm * -1

                impulse_1mm = ax.motorStepsPerRev / ax.fullStep_mm
                impulses = distance_mm * impulse_1mm * (1 << ax.controllerSettings["resolution"])
                self.tmcmAxis[axis].moveTo(int(round(impulses)))
                while self.tmcmAxis[axis].getActualVelocity() != 0:
                    if (self.tmcmAxis[axis].getErrorFlags() == 1):
                        LOGGER.warning(f"Warning, stall detected during axis {axis} moving!")
                    if pause_req.state == StateRequestsTypes.STOP:
                        LOGGER.info(f'Stopped Axis {axis}. Please Resume it to be able to move again')
                        return
                    await asyncio.sleep(0.2)
                LOGGER.info(f"Move {axis} with: {int(impulses)} impulses = {distance_mm} mm")
            else:
                LOGGER.warning(f'Axis {axis} cannot move before homed!')
        except Exception as e:
            LOGGER.error(f"Something went wrong (MoveAxis({axis}, {distance_mm} mm)):", str(e))

    def stop_axis(self, axis:str):
        """Immediately stops given 'axis'"""
        if (self._controller_connected == False):
            LOGGER.error(f"Warning: Action 'StopAxis()' refused! Axis {axis} not setup correctly!")
            raise RuntimeError
        pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
        self.tmcmAxis[axis].stop()
        pause_req.state = StateRequestsTypes.STOP
        LOGGER.info(f"Stopped axis {axis}")

    
    def resume_axis(self, axis: str):
        """Resumes the normal opperation and removes the stop request"""
        if self._controller_connected == False:
            LOGGER.error(f"Warning: Action 'resume_axis()' refused! Axis {axis} not setup correctly!")
            raise Exception
        pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
        pause_req.state = StateRequestsTypes.RESUME
        LOGGER.info(f"Resumes axis {axis}")


    def get_axis_coordinates(self, axis:str) -> float:
        try:
            if self._controller_connected == False:
                LOGGER.error(f"Get coordinates not possible Axis {axis} not setup correctly!")
                raise RuntimeError
            startingPointImpulses = 0
            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()

            if ax is not None:
                actualPosition = self.tmcmAxis[axis].getActualPosition()
                #LOGGER.info(f"Actual position on axis {axis} in impulses : {actualPosition}")
                if actualPosition != 0:
                    if (ax.controllerSettings["homeVelocity"] > 0):
                        startingPointImpulses = 4294967295 - actualPosition
                    else:
                        startingPointImpulses = actualPosition

                    impulse_1mm = ax.motorStepsPerRev / ax.fullStep_mm
                    coord = startingPointImpulses / (impulse_1mm * (1 << ax.controllerSettings["resolution"]))
                else:
                    coord = 0

            #LOGGER.info(f'Coordinates of axis {axis} in mm : {str(coord)}')
            return coord

        except ZeroDivisionError as e:
            LOGGER.error(f'Error while reading coordinates on axis {axis} : {e}')
        
    def get_axis_status(self, axis_id: str):
        status = list(filter(lambda x: x.name == axis_id, self._axes_status)).pop()
        status.coordinate = self.get_axis_coordinates(axis_id)
        return status

async def test():
    m = MovementControl()
    print(m)
    c = MovementControl()
    print(c)

    await m.build_axes()
    await c.home_axis('x')
    """
    LOGGER.info(m.get_axis_coordinates('x'))
    time.sleep(2)
    await c.home_axis('y')
    await c.move_axis('y', 20)
    LOGGER.info(m.get_axis_coordinates('y'))
    time.sleep(2)
    await c.home_axis('z')
    await c.move_axis('z', 20)

    LOGGER.info(m.get_axis_coordinates('z'))

    await c.move_axis('x', 150)
    await c.move_axis('y', 150)
    await c.move_axis('z', 50)

    print('Waiting for homin')
    while 1:
        time.sleep(10)
        LOGGER.info(m.get_axis_coordinates('x'))
    """
if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())
                 

















