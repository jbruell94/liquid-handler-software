from json.encoder import JSONEncoder
from unittest.mock import Mock
import json
from pathlib import Path


from config import motion_config as mc
from config import AXES_CONFIG_FILE

sample_motion_config = [
	{
		"name": "z",
		"safetyOffset": 20,
		"maxDeviation": 0,
		"fullStep_mm": 8,
		"additionalOffset": -9,
		"travelHightOffsetMm": 2,
		"startingPoint": 86,
		"jogVelocityForward": 50000,
		"jogVelocityBackward": -50000,
		"maxMovementmm": 100,
		"motorStepsPerRev": 200,
		"controllerSettings": {
			"controllerType": "TMCM_6214",
			"id": 3,
			"replyID": 0,
			"encoderAvailable": False,
			"resolution": 8,
			"runCurrent": 128,
			"standbyCurrent": 8,
			"PWMAutoScale": 0,
			"homeVelocity": 90800,
			"StallGuard2Treshold": 0,
			"StopOnStallVelocity": 60000,
			"Stallguard2Filter": 0
		},
		"rampSettings": {
			"startVelocity": 1,
			"accelerationA1": 24000,
			"velocityV1": 230400,
			"maxAccelerationA2": 500000,
			"maxVelocity": 460800,
			"maxDecelerationD2": 460800,
			"decelerationD1": 240000,
			"stopVelocity": 1,
			"rampWaitTime": 1
		}
	},
	{
		"name": "y",
		"safetyOffset": 20,
		"maxDeviation": 0,
		"fullStep_mm": 60,
		"additionalOffset": 0,
		"travelHightOffsetMm": 0,
		"startingPoint": 439,
		"jogVelocityForward": 50000,
		"jogVelocityBackward": -50000,
		"maxMovementmm": 460,
		"motorStepsPerRev": 200,
		"controllerSettings": {
			"controllerType": "TMCM_6214",
			"id": 1,
			"replyID": 0,
			"encoderAvailable": False,
			"resolution": 8,
			"runCurrent": 128,
			"standbyCurrent": 8,
			"PWMAutoScale": 0,
			"homeVelocity": 90800,
			"StallGuard2Treshold": 2,
			"StopOnStallVelocity": 60000,
			"Stallguard2Filter": 0
		},
		"rampSettings": {
			"startVelocity": 1,
			"accelerationA1": 24000,
			"velocityV1": 230400,
			"maxAccelerationA2": 500000,
			"maxVelocity": 460800,
			"maxDecelerationD2": 460800,
			"decelerationD1": 240000,
			"stopVelocity": 1,
			"rampWaitTime": 1
		}
	},
	{
		"name": "x",
		"safetyOffset": 20,
		"maxDeviation": 0,
		"fullStep_mm": 60,
		"additionalOffset": 0,
		"travelHightOffsetMm": 0,
		"startingPoint": 23,
		"jogVelocityForward": 50000,
		"jogVelocityBackward": -50000,
		"maxMovementmm": 460,
		"motorStepsPerRev": 200,
		"controllerSettings": {
			"controllerType": "TMCM_6214",
			"id": 2,
			"replyID": 0,
			"encoderAvailable": False,
			"resolution": 8,
			"runCurrent": 128,
			"standbyCurrent": 8,
			"PWMAutoScale": 0,
			"homeVelocity": -90800,
			"StallGuard2Treshold": 1,
			"StopOnStallVelocity": 60000,
			"Stallguard2Filter": 0
		},
		"rampSettings": {
			"startVelocity": 1,
			"accelerationA1": 24000,
			"velocityV1": 230400,
			"maxAccelerationA2": 500000,
			"maxVelocity": 460800,
			"maxDecelerationD2": 460800,
			"decelerationD1": 240000,
			"stopVelocity": 1,
			"rampWaitTime": 1
		}
	}
]



def test_default_config():
    jsonMock = Mock()
    jsonMock = {}
    config = mc._build_axes_config(jsonMock)

    ax = list(filter(lambda x: x.name == config[0].name, sample_motion_config)).pop()
    assert ax
    ax = list(filter(lambda x: x.name == config[1].name, sample_motion_config)).pop()
    assert ax
    ax = list(filter(lambda x: x.name == config[2].name, sample_motion_config)).pop()
    assert ax
    


def test_write_config():
    config = mc._build_axes_config(sample_motion_config)
    encoder = Mock(JSONEncoder)

    mc._write_config(config, AXES_CONFIG_FILE, encoder)


if __name__ == "__main__":
    test_default_config()
    test_write_config()