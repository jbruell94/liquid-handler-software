from PyTrinamic.connections.ConnectionManager import ConnectionManager
from driver.motors.TMCM_6214 import TMCM_6214
from driver.motors.TMCM_1241 import TMCM_1241

class AxisFactory():
    def __init__(self):
        pass

    @staticmethod
    def createAxis(controllerType: str, id: int, hostID: int):
        if (controllerType == "TMCM_6214"):
            connectionManager = ConnectionManager()
            connectionHandler = connectionManager.connect()
            return TMCM_6214(connectionHandler, id)
        elif (controllerType == "TMCM_1241"):
            connectionManager = ConnectionManager(argList="--interface socketcan_tmcl --data-rate 1000000 --host-id {hid} --module-id {mid}".format(mid = id, hid = hostID))
            connectionHandler = connectionManager.connect()
            return TMCM_1241(connectionHandler, id)
        elif (controllerType == "Simulation"):
            pass
        else:
            raise Exception("AxisFactory.createAxis(): Invalid controller type!")
