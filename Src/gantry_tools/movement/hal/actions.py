from dataclasses import dataclass, field
from typing import List
import uuid
from copy import deepcopy

@dataclass
class ActionParameter:
    name: str
    type: str
    show: str

@dataclass
class Action:
    name: str
    description: str
    parameters: List[ActionParameter]
    _id: str = field(default_factory=uuid.uuid4)

ACTIONS = [
    Action(name="home_axis",
            description="Homes one axis",
            parameters=[]),
    Action(name="home_all_axis",
            description="Homes all axes at once",
            parameters=[]),
    Action(name="jog_axis",
            description="Starts the movement of the given 'axis' in forward or backward direction",
            parameters=[ActionParameter(name="forward", type="bool", show="true")]),
    Action(name="move_to_location",
            description="Move the gantry Head to a specified location on Deck",
            parameters=[ActionParameter(name="x_axis_loc", type="float", show="false"), 
                        ActionParameter(name="y_axis_loc", type="float", show="false"), 
                        ActionParameter(name="z_axis_loc", type="float", show="false")]),
    Action(name="move_relative",
            description="Move given axis relatively by given 'distance_mm'",
            parameters=[ActionParameter(name="distance_mm", type="float", show="true")]),
    Action(name="move_absolute",
            description="Move given axis to absolute position by given 'distance_mm'",
            parameters=[ActionParameter(name="distance_mm", type="float", show="true")]),
    Action(name="stop_axis",
            description="Immediately stops given axis.",
            parameters=[]),
    Action(name="resume_axis",
            description="Resumes the normal opperation and removes the stop request.",
            parameters=[]),
    Action(name="get_axis_coordinates",
            description="Returns the coordinates of a given axis",
            parameters=[]),
    Action(name="is_axis_homed",
            description="Returns whether an axis is homed or not",
            parameters=[]),
    Action(name="add_offset",
            description="Add an offset to the movement of an axis in mm. Sign sets the direction of offset.",
            parameters=[ActionParameter(name="offset", type="float", show="true")]),
    Action(name="remove_offset",
            description="Remove the offset if set beforehand",
            parameters=[])            
]

def get_actions():
    actions_copy = deepcopy(ACTIONS)
    action_dict_list = [action.__dict__ for action in actions_copy]
    for action in action_dict_list:
        action['parameters'] = [parameter.__dict__ for parameter in action['parameters']]

    return action_dict_list     
