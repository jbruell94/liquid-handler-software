import asyncio
from dis import dis
from typing import List
import uuid

from hal import LOGGER
from hal.statusManager.MoveStateManager import MovementStateManager, StateRequestsTypes
from hal.axisFactory import AxisFactory
import config.motion_config as mc
from hal.statusManager.StatusDataTypes import AxisStatus, AxisStatusTypes
from hal.ExceptionTypes import AxesNotSetupError, WrongAxisRequestedError, ConfigNotLoadedError
class MovementControl():
    """This module contains the basic movement 
    functions availabe for this gantry
    The module uses the asincio package in 
    order not to block the execution while moving
    This must be a singleton class object

    @param: loop- the async event to be used in this module
    """
    _instance = None
    _controller_connected = False
    _axes_status: List[AxisStatus] = []
    _stopAxisReq: bool = False
    _move_req: List[MovementStateManager] = []
    _VERSION = "0.0.1"
    _NAME = "MOVEMENT"
    _ID = uuid.uuid4()

    def __new__(cls):
        if cls._instance is None:
            LOGGER.info("creating Movement control class. Using only this instance from now on!")
            cls._instance = super(MovementControl, cls).__new__(cls)
        return cls._instance
    
    def __init__(self) -> None:
        self._axisSetup = False
        self._stopAxisReq = False
        self.axis_offset = {}
        self.toolConfig = mc.get_config()
        if self.toolConfig is None:
            LOGGER.error("Cannot create motion control object. Cannot load config!")
            raise ConfigNotLoadedError
        
        LOGGER.info(f'Loaded the following configuration {self.toolConfig}')
        self.tmcmAxis = {}

    @property
    def axes_status(self):
        return self._axes_status

    @property
    def version(self):
        return self._VERSION

    @property
    def id(self):
        return self._ID

    @property
    def name(self):
        return self._NAME

    @property
    def dimensions(self):
        return self.toolConfig.dimensions.__dict__

    @property
    def referencePoint(self):
        return self.toolConfig.referencePoint


    async def build_axes(self):      
        self._move_req = []
        self._axes_status = []
        try: 
            for config in self.toolConfig.axesConfig:
                self.tmcmAxis[config.name] = AxisFactory.createAxis(controllerType=config.controllerSettings.get('controllerType', {}), 
                                                                    id=config.controllerSettings.get('id', {}), 
                                                                    hostID=config.controllerSettings.get('replyID', {}))
                #add axes while still in Init mode
                self._axes_status.append(AxisStatus(config.name, status=AxisStatusTypes.IDLE))
                #add pause manager for each axis
                self._move_req.append(MovementStateManager(config.name, StateRequestsTypes.IDLE))

                LOGGER.info(f'{self.tmcmAxis[config.name]},  {config.name}, {config}')
                await self._configure_axis(self.tmcmAxis[config.name], config) 
                #set offset to 0
                self.axis_offset[config.name] = 0

            self._controller_connected = True          
        except ValueError:
            LOGGER.error(f'error in configuration')  
        except Exception as e:
            LOGGER.error(f'Axes cannot be initialized: {e}')
            LOGGER.info('Please check connection, configuration, then reboot and try again!')

    async def _configure_axis(self, tmcm, axisConfig):
        """Configure each axis in the Trinamic controller

        @param: tmcm -> 
        """
        if tmcm: 
            tmcm.setStallGuard2Treshold(axisConfig.controllerSettings.get("StallGuard2Treshold", 0))
            tmcm.setStopOnStallVelocity(axisConfig.controllerSettings.get("StopOnStallVelocity", 0))
            tmcm.setStallguard2Filter(axisConfig.controllerSettings.get("Stallguard2Filter", 0))   
            tmcm.setMaxVelocity(int(axisConfig.rampSettings.get("maxVelocity", 0)))
            tmcm.setMaxAcceleration(axisConfig.rampSettings.get("maxAccelerationA2", 0))
            tmcm.setMotorMicroStepResolution(axisConfig.controllerSettings.get("resolution", 0))
            # TODO adapt regarding resolution
            # self.axises[axisKey].setSmartEnergyThresholdSpeed(76800)
            # self.axises[axisKey].setPWMThresholdSpeed(76800)
            # self.axises[axisKey].setPWMGrad(15)
            # self.axises[axisKey].setPWMAmplitude(65)
            # self.axises[axisKey].setPWMAutoscale(self.axisConfig[axisKey].PWMAutoScale)
            tmcm.setMotorMaxRunCurrent(axisConfig.controllerSettings.get("runCurrent", 0))
            tmcm.setMotorStandbyCurrent(axisConfig.controllerSettings.get("standbyCurrent", 0))
            # Setup RAMP settings
            tmcm.setStartVelocity(axisConfig.rampSettings.get("startVelocity", 0))
            tmcm.setA1(axisConfig.rampSettings.get("accelerationA1", 0))
            tmcm.setV1(axisConfig.rampSettings.get("velocityV1", 0))
            tmcm.setMaxAcceleration(axisConfig.rampSettings.get("maxAccelerationA2", 0))
            tmcm.setMaxVelocity(axisConfig.rampSettings.get("maxVelocity", 0))
            tmcm.setStartVelocity(axisConfig.rampSettings.get("startVelocity", 0))
            tmcm.setMaxDeceleration(axisConfig.rampSettings.get("maxDecelerationD2", 0))
            tmcm.setD1(axisConfig.rampSettings.get("decelerationD1", 0))
            tmcm.setStopVelocity(axisConfig.rampSettings.get("stopVelocity", 0))
            tmcm.setRampWaitTime(axisConfig.rampSettings.get("rampWaitTime", 0))
            tmcm.setMaxEncoderDeviation(axisConfig.maxDeviation)
            tmcm.stop()
        else:
            raise ValueError     

    async def home_axis(self, axis: str):

        """ 
        Homes 1 axis with id
        It uses the stallguard configuration to stop the motors at end point.         
        When the axis is home, it sets the actual possition of the axis to zero
        As a finel step it moves the axis at offset from 0

        @param axis:str -> this parameter is dependent on the configuration parameter 'name'

        @raises: AxesNotSetupError -> if not even one axis is connected, will raise this error
        @raises: WrongAxisRequestedError -> if the requested axis is not available
        """
        if (self._controller_connected == False):
            LOGGER.error('Axis is nost setup. Please try again after successful connection')
            raise AxesNotSetupError

        ax = self.get_axis_by_id(axis)
        status = self.get_axis_status_by_id(axis)

        # Setup  one axis at a time
        if ax and status:
            #clear axis errors
            LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')
            self.tmcmAxis[axis].rotate(ax.controllerSettings["homeVelocity"])
            status.status = AxisStatusTypes.HOMING
            LOGGER.info(f'Started Homing of axis {axis}')

            while (self.tmcmAxis[axis].getActualVelocity() != 0 and self.tmcmAxis[axis].getErrorFlags() == 0):
                await asyncio.sleep(0.1)
                
            self.tmcmAxis[axis].setActualPosition(0)

            await asyncio.sleep(1)

            self.tmcmAxis[axis].stop()
            status.status = AxisStatusTypes.IDLE
            status.homed = True
            try:
                await self.move_axis(axis, ax.safetyOffset)
            except:
                LOGGER.error(f'Homing of axis {axis} FAILED!, Please try homing again')

            #LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')
            #LOGGER.info(f'Load Values: {self.tmcmAxis[axis].getLoadValue()}')
            
            if ax.controllerSettings["encoderAvailable"]:
                self.tmcmAxis[axis].setEncoderPosition(0)

        else:
            LOGGER.error(f'Requested wrong axis {ax}')
            raise WrongAxisRequestedError



    async def jog_axis(self, axis: str, forward: bool):
        """Starts the movement of the given 'axis' in forward or backward direction with velocity set in the configuration
        Velocity for forward and backward could be set with different values
        
        @param: axis - one of the names from the configuration file
        @param: forward - true for moving in the forward direction with positive velocity

        @raises: AxesNotSetupError -> if no axis is connected
        """
        if (self._controller_connected == False):
            LOGGER.error('Axis is nost setup. Please try again after successful connection')
            raise AxesNotSetupError

        ax = self.get_axis_by_id(axis)
        status = self.get_axis_status_by_id(axis)
        pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()

        if not ax or not status:
            raise WrongAxisRequestedError
        
        if pause_req.state != StateRequestsTypes.STOP:
            status.status = AxisStatusTypes.MOVING
            if forward:
                self.tmcmAxis[axis].rotate(ax.jogVelocityForward)
                while pause_req.state != StateRequestsTypes.STOP:
                    if (self.tmcmAxis[axis].getErrorFlags() == 1):
                        LOGGER.warn(f'Hit the end of the gantry! Stopping Axis')
                        self.stop_axis(axis)
                    await asyncio.sleep(0.1)
            else:
                self.tmcmAxis[axis].rotate(ax.jogVelocityBackward)
                while pause_req.state != StateRequestsTypes.STOP:
                    if (self.tmcmAxis[axis].getErrorFlags() == 1):
                        LOGGER.warn(f'Hit the end of the gantry! Stopping Axis')
                        self.stop_axis(axis)
                    await asyncio.sleep(0.1)

            status.status = AxisStatusTypes.IDLE


    async def move_axis(self, axis:str, distance_mm: float):
        """
        Move given 'axis' relatively by given 'distance_mm'.
        automatic conversion is done from mm to steps
        @param name: from configuration file
        @param distance_mm: distance to move in mm. Will not move if the distance is larger that 
            that max movement
        """
        if self._controller_connected == False:
            LOGGER.error(f"Axis not setup. Cannot move {axis}")
            raise AxesNotSetupError

        ax = self.get_axis_by_id(axis)
        pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
        #TODO This idle will check for all three axes together
        status = self.get_axis_status_by_id(axis)
        if ax and status:
            if status.status == AxisStatusTypes.IDLE and status.homed:
            
                distance_mm = distance_mm - self.axis_offset[axis]

                if distance_mm < 0:
                    distance_mm = 0

                if((ax.maxMovementmm) < distance_mm):
                    LOGGER.error(f'Requested move is beyond the capabilities of the axis {axis}')
                    return 

                # Due to always positive value for input distance, multiply by -1 depending on home velocity sign
                if ax.controllerSettings["homeVelocity"] > 0:
                    distance_mm = distance_mm * -1
                                
                try:
                    impulse_1mm = ax.motorStepsPerRev / ax.fullStep_mm
                except ZeroDivisionError:
                    LOGGER.error(f"Tried to divide by zero when moving axis: {axis}")
                    return

                impulses = distance_mm * impulse_1mm * (1 << ax.controllerSettings["resolution"])
                self.tmcmAxis[axis].moveTo(int(round(impulses)))

                while self.tmcmAxis[axis].getActualVelocity() != 0:
                    status.status = AxisStatusTypes.MOVING
                    if (self.tmcmAxis[axis].getErrorFlags() == 1):
                        LOGGER.warning(f"Warning, stall detected during axis {axis} moving!")
                    if pause_req.state == StateRequestsTypes.STOP:
                        LOGGER.info(f'Stopped Axis {axis}. Please Resume it to be able to move again')
                        status.status = AxisStatusTypes.IDLE
                        return
                    await asyncio.sleep(0.2)
                status.status = AxisStatusTypes.IDLE
                LOGGER.info(f"Move {axis} with: {int(impulses)} impulses = {distance_mm} mm")
            else:
                LOGGER.warning(f'Axis {axis} cannot move before homed!')

                
        else:
            raise WrongAxisRequestedError

    def stop_axis(self, axis:str):
        """Immediately stops given 'axis'. After the axis has been stopped, it must be resumed trough resume_axis
        @param: axis -> name of the axis to stop

        @raises: AxesNotSetupError -> if no axis is setup correctly
        @raises: WrongAxisRequestedError -> no such axis is configured           
        """
        if (self._controller_connected == False):
            LOGGER.error(f"Warning: Action 'StopAxis()' refused! Axis {axis} not setup correctly!")
            raise AxesNotSetupError

        ax = self.get_axis_by_id(axis)
        if ax:
            pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
            self.tmcmAxis[axis].stop()
            pause_req.state = StateRequestsTypes.STOP
            LOGGER.info(f"Stopped axis {axis}")
        else:
            raise WrongAxisRequestedError
    
    def resume_axis(self, axis: str):
        """Resumes the normal opperation and removes the stop request

        @param: axis -> name of the axis to stop

        @raises: AxesNotSetupError -> if no axis is setup correctly 
        @raises: WrongAxisRequestedError -> no such axis is configured           
        """
        if self._controller_connected == False:
            LOGGER.error(f"Warning: Action 'resume_axis()' refused! Axis {axis} not setup correctly!")
            raise AxesNotSetupError

        ax = self.get_axis_by_id(axis)
        if ax:
            pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
            pause_req.state = StateRequestsTypes.RESUME
            LOGGER.info(f"Resumes axis {axis}")
        else:
            raise WrongAxisRequestedError

    def get_axis_coordinates(self, axis:str) -> float:
        """Get the current coordinates of the requested axis

        @param: axis -> name of the axis to stop

        @raises: AxesNotSetupError -> if no axis is setup correctly    
        @raises: WrongAxisRequestedError -> no such axis is configured    
        """
        if self._controller_connected == False:
            LOGGER.error(f"Get coordinates not possible Axis {axis} not setup correctly!")
            raise AxesNotSetupError
            
        startingPointImpulses = 0
        ax = self.get_axis_by_id(axis)
        coord = 0
        if ax:
            actualPosition = self.tmcmAxis[axis].getActualPosition()
            #LOGGER.info(f"Actual position on axis {axis} in impulses : {actualPosition}")
            if actualPosition != 0:
                if (ax.controllerSettings["homeVelocity"] > 0):
                    startingPointImpulses = 4294967295 - actualPosition
                else:
                    startingPointImpulses = actualPosition
                try:
                    impulse_1mm = ax.motorStepsPerRev / ax.fullStep_mm
                    coord = startingPointImpulses / (impulse_1mm * (1 << ax.controllerSettings["resolution"]))
                except ZeroDivisionError:
                    LOGGER.error(f'Tried to divide by zero when getting coordinates on axis : {axis}')
            else:
                coord = 0
        else:
            raise WrongAxisRequestedError
        #LOGGER.info(f'Coordinates of axis {axis} in mm : {str(coord)}')

        #add offset
        
        return coord + self.axis_offset[axis]

    def get_axis_status(self, axis_id: str):
        status = self.get_axis_status_by_id(axis_id)
        status.coordinate = self.get_axis_coordinates(axis_id)
        status.driver_status = self.read_driver_status(axis_id)
        return status

    def read_driver_status(self, axis_id: str):

        driver_status = self.tmcmAxis[axis_id].getDriverErrorString() + self.tmcmAxis[axis_id].getErrorStatusString()

        return 'Normal operation' if driver_status == '' else driver_status

    def is_driver_error_active(self):
        for axis in self.tmcmAxis:
            if (self.tmcmAxis[axis].isAxisInErrorState()):
                return True
        
        return False

    def get_axis_serial_number(self, axis_id: str):
        axis = self.get_axis_by_id(axis_id)
        if axis:
            serial_number = axis.controllerSettings['id']
        else:
            raise WrongAxisRequestedError
        
        return serial_number
    
    def get_axis_by_id(self, axis_id: str):
        ax = None
        try:
            ax = list(filter(lambda x: x.name == axis_id, self.toolConfig.axesConfig)).pop()
        except Exception as e:
            LOGGER.error(f"Could not find the requested axis :{e}")

        return ax

    def get_axes_names(self) -> List[str]:
        return list(filter(lambda x: x.name, self.toolConfig.axesConfig))

    def get_axis_status_by_id(self, axis_id: str):
        status = None
        try:
            status = list(filter(lambda x: x.name == axis_id, self._axes_status)).pop()
        except:
            LOGGER.error("Could not find the status requested axis")

        return status

    def set_axis_offset(self, axis_id: str, offset: float):
        try:
            self.axis_offset[axis_id] = offset
        except:
            LOGGER.error(f"Could not set offset of axis {axis_id}")

    async def reload_config(self):
        self.toolConfig = mc.get_config()
        await self.build_axes()

async def test():
    m = MovementControl()
    print(m)
    c = MovementControl()
    print(c)

    await m.build_axes()
    await c.home_axis('x')
    """
    LOGGER.info(m.get_axis_coordinates('x'))
    time.sleep(2)
    await c.home_axis('y')
    await c.move_axis('y', 20)
    LOGGER.info(m.get_axis_coordinates('y'))
    time.sleep(2)
    await c.home_axis('z')
    await c.move_axis('z', 20)

    LOGGER.info(m.get_axis_coordinates('z'))

    await c.move_axis('x', 150)
    await c.move_axis('y', 150)
    await c.move_axis('z', 50)

    print('Waiting for homin')
    while 1:
        time.sleep(10)
        LOGGER.info(m.get_axis_coordinates('x'))
    """
if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(test())
                 

















