from __future__ import annotations
import asyncio
import time
from typing import List

from hal.statusManager.StatusDataTypes import AxisStatus
from hal import LOGGER
from hal.MoveControl import MovementControl
class MovementStatus():
    """
    This is the context class that handles context switches and state methods
    """
    _motion_control_instance: MovementControl = None
    _status_ax: List[AxisStatus] = None
    _timeout: int = 0.5
    _use_asyncio: bool = False

    def __init__(self, mv_control_instance, timeout: int = 0.5, use_asyncio: bool = False):
        self._motion_control_instance = mv_control_instance
        self._status_ax = None
        self._timeout = timeout
        self._use_asyncio = use_asyncio
        self._register_axes()

    def _register_axes(self):
        if self._motion_control_instance.axes_status is not None:
            self._status_ax = self._motion_control_instance.axes_status
        else:
            LOGGER.warning('Could not register axes status. Please check configuration files!')

    @property
    def status(self):
        status_dict = {"Movement status": list(map(lambda x: x.__dict__, self._status_ax))}
        return status_dict
    
    async def update_status(self):
        if self._status_ax is not None:
            for i in range(0, len(self._status_ax)):
                self._status_ax[i] = self._motion_control_instance.get_axis_status(self._status_ax[i].name)
                # self._status_ax[i].status = str(self._status_ax[i].status)
            
    async def run(self):
        while True:
            await self.update_status()
            
            if self._use_asyncio:
                await asyncio.sleep(self._timeout)
            else:
                time.sleep(self._timeout)

            

