from dataclasses import dataclass
from typing import List, NamedTuple, TypedDict

class extControlSettings(TypedDict):
    controllerType: str
    id: int
    replyID: int
    encoderAvailable: bool
    resolution: int
    runCurrent: int
    standbyCurrent: int
    PWMAutoScale: int
    homeVelocity: int
    StallGuard2Treshold: int
    StopOnStallVelocity: int
    Stallguard2Filter: int

class RampSettings(NamedTuple):
    startVelocity: int
    accelerationA1: int
    velocityV1: int
    maxAccelerationA2: int
    maxVelocity: int
    maxDecelerationD2: int
    decelerationD1: int
    stopVelocity: int
    rampWaitTime: int

@dataclass
class AxisConfig():
    name: str
    safetyOffset: int
    maxDeviation: int
    fullStep_mm: int
    additionalOffset: int
    travelHightOffsetMm: int
    startingPoint: int
    jogVelocityForward: int
    jogVelocityBackward: int
    maxMovementmm: int
    motorStepsPerRev: int
    controllerSettings: extControlSettings
    rampSettings: RampSettings

@dataclass
class Dimensions:
    xDimension: float
    yDimension: float
    zDimension: float

@dataclass
class ToolConfig():
    dimensions: Dimensions
    referencePoint: str
    axesConfig: List[AxisConfig]