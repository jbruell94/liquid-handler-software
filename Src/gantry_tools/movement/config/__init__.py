import sys
from pathlib import Path
import logging
from datetime import datetime

_AXES_CONFIG_NAME           = "axis_config.json"
_CALIBRATION_CONFIG_NAME    = "calibration_config.json"

_CONFIG_DIR     = ".lhm_config"
_LOGGING_DIR    = "lhm_log"
_LOGGING_FILE   = 'lhm_config_log'
CONFIG_LOG = object


IS_LINUX        = sys.platform.startswith("linux")
IS_WINDOWS      = sys.platform.startswith("win")

def make_config_dir() -> Path:    
    home = Path.home()
    if Path(home, _CONFIG_DIR).exists():
        pass
    else:
        Path.mkdir(Path(home, _CONFIG_DIR))
    return Path(home, _CONFIG_DIR)

def make_config_file(dir: Path, name: str):
    if Path(dir, name).exists():
        pass
    else:   
        Path(dir, name).touch()
    return Path(dir, name)

def _setup_log(logging_path: Path) -> logging.Logger:


    if not Path(logging_path, _LOGGING_DIR).exists():
        Path.mkdir(Path(logging_path, _LOGGING_DIR))

    if Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).exists():
        Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).unlink()

    Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).touch()
    file = str(Path(logging_path, _LOGGING_DIR, _LOGGING_FILE))

    _handler = logging.FileHandler(file)
    logger = logging.getLogger('config_logger')
    logger.setLevel(logging.INFO)
    logger.addHandler(_handler)
    
    #logging.basicConfig(filename=file, level=logging.DEBUG)
    logger.info(f'Logging Started at {datetime.now()}')
    return logger


CONFIG = make_config_dir()
AXES_CONFIG_FILE = make_config_file(CONFIG, _AXES_CONFIG_NAME)
CALIBRATION_CONFIG_FILE = make_config_file(CONFIG, _CALIBRATION_CONFIG_NAME)


CONFIG_LOG = _setup_log(Path(Path.home(), _CONFIG_DIR))
CONFIG_LOG.info("Started logging")