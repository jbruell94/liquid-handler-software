class TMCM_6214():
    MOTORS = 6

    # string at index 0 matches the error when bit 0 is set, the one at index 1 - when bit 1 is set, etc.
    __driver_error_status_string = (
        "Stall detected",
        "Driver is shut down due to overtemperature",
        "Temperature threshold is exceeded",
        "Short condition detected, driver currently shut down (A)",
        "Short condition detected, driver currently shut down (B)",
        "No chopper event has happened during the last period with constant coil polarity (A)",
        "No chopper event has happened during the last period with constant coil polarity (B)",
        "No step pulse occurred during the last 2^20 clock cycles"
    )

    def __init__(self, connection=None, motorID=0):
        self.connection = connection
        
        self.GPs   = _GPs
        self.APs   = _APs
        self.ENUMs = _ENUMs

        self.__default_motor = motorID

    @staticmethod
    def getEdsFile():
        # return __file__.replace("TMCM_6214.py", "TMCM_6214_V.3.22.eds")
        print("Not available for this module !")

    @staticmethod
    def showChipInfo():
        print("""The TMCM-6214 is a six axis controller/driver module for 2-phase bipolar stepper motors. 
        The TMCM-6214 TMCL 1rmware allows to control the module using TMCL™ commands, supporting
        standalone operation as well as direct mode control, making use of the Trinamic TMC5160 motion
        controller and motor driver. Dynamic current control, and quiet, smooth and eZcient operation
        are combined with StealthChop™, DcStep™, StallGuard2™ and CoolStep™ features.
        Supply voltage 9. . . 28V DC""")

    def getMotorID(self):
        return self.__default_motor
        
    # Axis parameter access
    def getAxisParameter(self, apType):
        return self.connection.axisParameter(apType, self.__default_motor)

    def setAxisParameter(self, apType, value):
        self.connection.setAxisParameter(apType, self.__default_motor, value)

    # Global parameter access
    def getGlobalParameter(self, gpType, bank):
        return self.connection.globalParameter(gpType, bank)

    def setGlobalParameter(self, gpType, bank, value):
        self.connection.setGlobalParameter(gpType, bank, value)

    # Motion Control functions
    def rotate_ror(self, velocity):
        self.connection.rotate(self.__default_motor, velocity)

    def rotate(self, velocity):
        self.setTargetVelocity(velocity)

    def stop(self):
        self.rotate(0)

    def moveTo(self, position, velocity=None):
        if velocity:
            self.setMaxVelocity(velocity)

        self.connection.move(0, self.__default_motor, position)
        self.setTargetPosition(position)

    def moveBy(self, difference, velocity=None):
        position = difference + self.getActualPosition()

        self.moveTo(position, velocity)

        return position

    # Current control functions
    def setMotorMaxRunCurrent(self, current):
        self.setAxisParameter(self.APs.MaxCurrent, current)

    def getMotorMaxRunCurrent(self):
        return self.getAxisParameter(self.APs.MaxCurrent)

    def setMotorStandbyCurrent(self, current):
        self.setAxisParameter(self.APs.StandbyCurrent, current)

    def getMotorStandbyCurrent(self):
        return self.getAxisParameter(self.APs.StandbyCurrent)

    def setMotorMicroStepResolution(self, microstepResolution):
        self.setAxisParameter(self.APs.MicrostepResolution, microstepResolution)

    def getMotorMicroStepResolution(self):
        return self.getAxisParameter(self.APs.MicrostepResolution)
        
    def getSmartEnergyActualCurrent(self):
        return self.getAxisParameter(self.APs.smartEnergyActualCurrent)

    def getPWMScale(self):
        return self.getAxisParameter(self.APs.PWMScale)

    def setPWMAutoscale(self, value):
        return self.setAxisParameter(self.APs.PWMAutoscale, value)

    def getPWMAutoscale(self):
        return self.getAxisParameter(self.APs.PWMAutoscale)

    # StallGuard2 Functions
    def setStallguard2Filter(self, enableFilter):
        self.setAxisParameter(self.APs.SG2FilterEnable, enableFilter)

    def getStallguard2Filter(self):
        return self.setAxisParameter(self.APs.SG2FilterEnable)

    def setStallGuard2Treshold(self, treshold = 0):
        self.setAxisParameter(self.APs.SG2Threshold, treshold)

    def getStallGuard2Treshold(self):
        return self.getAxisParameter(self.APs.SG2Threshold)

    def setStopOnStallVelocity(self, velocity = 0):
        self.setAxisParameter(self.APs.StopOnStallVelocity, velocity)

    def getStopOnStallVelocity(self):
        return self.getAxisParameter(self.APs.StopOnStallVelocity)
    
    def getLoadValue(self):
        return self.getAxisParameter(self.APs.LoadValue)

    # Motion parameter functions
    def getTargetPosition(self):
        return self.getAxisParameter(self.APs.TargetPosition)

    def setTargetPosition(self, position):
        self.setAxisParameter(self.APs.TargetPosition, position)

    def getActualPosition(self):
        return self.getAxisParameter(self.APs.ActualPosition)

    def setActualPosition(self, position):
        return self.setAxisParameter(self.APs.ActualPosition, position)

    def getTargetVelocity(self):
        return self.getAxisParameter(self.APs.TargetVelocity)

    def setTargetVelocity(self, velocity):
        self.setAxisParameter(self.APs.TargetVelocity, velocity)

    def getActualVelocity(self):
        return self.getAxisParameter(self.APs.ActualVelocity)

    def getMaxVelocity(self):
        return self.getAxisParameter(self.APs.MaxVelocity)

    def setMaxVelocity(self, velocity):
        self.setAxisParameter(self.APs.MaxVelocity, velocity)

    def getMaxAcceleration(self):
        return self.getAxisParameter(self.APs.MaxAcceleration)

    def setMaxAcceleration(self, acceleration):
        self.setAxisParameter(self.APs.MaxAcceleration, acceleration)

    def getRampMode(self):
        return self.getAxisParameter(self.APs.RampMode)

    def setRampMode(self, mode):
        return self.setAxisParameter(self.APs.RampMode, mode)

    def setSmartEnergyThresholdSpeed(self, speed):
        return self.setAxisParameter(self.APs.smartEnergyThresholdSpeed, speed)
        
    def getSmartEnergyThresholdSpeed(self):
        return self.getAxisParameter(self.APs.smartEnergyThresholdSpeed)

    def setPWMThresholdSpeed(self, speed):
        return self.setAxisParameter(self.APs.PWMThresholdSpeed, speed)
        
    def getPWMThresholdSpeed(self):
        return self.getAxisParameter(self.APs.PWMThresholdSpeed)

    def setPWMGrad(self, gradient):
        return self.setAxisParameter(self.APs.PWMGrad, gradient)
        
    def getPWMGrad(self):
        return self.getAxisParameter(self.APs.PWMGrad)

    def getPwmMode(self):
        return self.getAxisParameter(self.APs.PwmMode)

    def setPWMAmplitude(self, amplitude):
        return self.setAxisParameter(self.APs.PWMAmplitude, amplitude)

    def getPWMAmplitude(self):
        return self.getAxisParameter(self.APs.PWMAmplitude)

    def setDcStepStallGuard(self, sensitivity):
        return self.setAxisParameter(self.APs.DcStepStallGuard, sensitivity)

    def getDcStepStallGuard(self):
        return self.getAxisParameter(self.APs.DcStepStallGuard)

    def setPowerDownDelay(self, mSeconds):
        return self.setAxisParameter(self.APs.PowerDownDelay, mSeconds)

    def getPowerDownDelay(self):
        return self.getAxisParameter(self.APs.PowerDownDelay)

    def setA1(self, velocity):
        return self.setAxisParameter(self.APs.A1, velocity)

    def getA1(self):
        return self.getAxisParameter(self.APs.A1)

    def setV1(self, velocity):
        return self.setAxisParameter(self.APs.V1, velocity)

    def getV1(self):
        return self.getAxisParameter(self.APs.V1)

    def setMaxDeceleration(self, velocity):
        return self.setAxisParameter(self.APs.MaxDeceleration, velocity)

    def getMaxDeceleration(self):
        return self.getAxisParameter(self.APs.MaxDeceleration)

    def setD1(self, velocity):
        return self.setAxisParameter(self.APs.D1, velocity)

    def getD1(self):
        return self.getAxisParameter(self.APs.D1)

    def setStartVelocity(self, velocity):
        return self.setAxisParameter(self.APs.StartVelocity, velocity)

    def getStartVelocity(self):
        return self.getAxisParameter(self.APs.StartVelocity)

    def setStopVelocity(self, velocity):
        return self.setAxisParameter(self.APs.StopVelocity, velocity)

    def getStopVelocity(self):
        return self.getAxisParameter(self.APs.StopVelocity)

    def setRampWaitTime(self, times_s):
        return self.setAxisParameter(self.APs.RampWaitTime, times_s)

    def getRampWaitTime(self):
        return self.getAxisParameter(self.APs.RampWaitTime)

    def setUnitMode(self, value):
        return self.setAxisParameter(self.APs.UnitMode, value)

    def getUnitMode(self):
        return self.getAxisParameter(self.APs.UnitMode)

    # Encoder functions
    def setMaxEncoderDeviation(self, deviation):
        self.setAxisParameter(self.APs.max_EncoderDeviation, deviation)

    def getEncoderPosition(self):
        return self.getAxisParameter(self.APs.EncoderPosition, signed=True)

    def setEncoderPosition(self, position):
        self.setAxisParameter(self.APs.EncoderPosition, position)

    # Status functions
    def getStatusFlags(self):
        return self.getAxisParameter(self.APs.TMC262ErrorFlags)

    def getErrorFlags(self):
        return self.getAxisParameter(self.APs.extendedErrorFlags)

    def positionReached(self):
        return self.getAxisParameter(self.APs.PositionReachedFlag)

    def getDrvStatusFlags(self):
        return self.getAxisParameter(self.APs.DrvStatusFlags)

    # IO pin functions
    def analogInput(self, x):
        return self.connection.analogInput(x)

    def digitalInput(self, x):
        return self.connection.digitalInput(x)

    def getDriverErrorString(self):
        driverErrorFlags = self.getDrvStatusFlags()

        description = ''
        currentBitPos = 0
        while (driverErrorFlags):
            if (driverErrorFlags & 1):
                description += self.__driver_error_status_string[currentBitPos]
                description += '\n'
            
            driverErrorFlags >>= 1
            currentBitPos += 1
        
        return description

    def getErrorStatusString(self):
        errorFlags = self.getErrorFlags()
        description = ''

        if (errorFlags == 1):
            description = "StallGuard error"
        elif (errorFlags == 2):
            description = "Deviation error"
        
        return description

    def isAxisInErrorState(self):
        has_error = self.getErrorFlags()

        # We exclude the "Stand still" status since it is not actually an error
        has_error |= (self.getDrvStatusFlags() & ~(1 << 7))

    def showMotionConfiguration(self):
        print("Motion configuration:")
        print("\tMax velocity: " + str(self.getMaxVelocity()))
        print("\tAcceleration: " + str(self.getMaxAcceleration()))
        print("\tRamp mode: " + ("position" if (self.getRampMode()==0) else "velocity"))


class _APs():
    TargetPosition                 = 0
    ActualPosition                 = 1
    TargetVelocity                 = 2
    ActualVelocity                 = 3
    MaxVelocity                    = 4  #Maximum positioning speed
    MaxAcceleration                = 5 # VA
    MaxCurrent                     = 6  # Motor current used when motor is running.
    StandbyCurrent                 = 7
    PositionReachedFlag            = 8
    HomeSwitch                     = 9
    RightEndstop                   = 10
    LeftEndstop                    = 11
    AutomaticRightStop             = 12
    AutomaticLeftStop              = 13
    swapSwitchInputs               = 14
    A1                             = 15 # Acceleration A1
    V1                             = 16 # Velocity V1
    MaxDeceleration                = 17 # D2
    D1                             = 18 # Deceleration D1
    StartVelocity                  = 19
    StopVelocity                   = 20
    RampWaitTime                   = 21
    THIGH                          = 22
    VDCMIN                         = 23
    rightSwitchPolarity            = 24
    leftSwitchPolarity             = 25
    softstop                       = 26
    HighSpeedChopperMode           = 27
    HighSpeedFullstepMode          = 28
    MeasuredSpeed                  = 29
    PowerDownRamp                  = 31
    DcStepStallGuard               = 33
    RelativePositioningOptionCode  = 127
    MicrostepResolution            = 140    # 0..8;
    ChopperBlankTime               = 162
    ConstantTOffMode               = 163
    DisableFastDecayComparator     = 164
    ChopperHysteresisEnd           = 165
    ChopperHysteresisStart         = 166
    TOff                           = 167
    SEIMIN                         = 168
    SECDS                          = 169
    smartEnergyHysteresis          = 170
    SECUS                          = 171
    smartEnergyHysteresisStart     = 172
    SG2FilterEnable                = 173    # 0/1
    SG2Threshold                   = 174    # -64. . . +63
    ShortToGroundProtection        = 177
    VSense                         = 179
    smartEnergyActualCurrent       = 180
    StopOnStallVelocity            = 181    # 0. . . 7999774[pps]
    smartEnergyThresholdSpeed      = 182
    RandomTOffMode                 = 184
    ChopperSynchronization         = 185
    PWMThresholdSpeed              = 186
    PWMGrad                        = 187
    PWMAmplitude                   = 188
    PWMScale                       = 189
    PwmMode                        = 190
    PWMFrequency                   = 191
    PWMAutoscale                   = 192
    ReferenceSearchMode            = 193
    ReferenceSearchSpeed           = 194
    RefSwitchSpeed                 = 195
    RightLimitSwitchPosition       = 196
    LastReferencePosition          = 197
    encoderMode                    = 201
    MotorFullStepResolution        = 202
    pwmSymmetric                   = 203
    FreewheelingMode               = 204
    LoadValue                      = 206
    extendedErrorFlags             = 207    # 0-3 / 1-StallGuard error 2-deviation error
    DrvStatusFlags                 = 208
    EncoderPosition                = 209
    EncoderResolution              = 210
    max_EncoderDeviation           = 212
    PowerDownDelay                 = 214
    UnitMode                       = 255

class _ENUMs():
    pass

class _GPs():

    CANBitrate                    = 69
    CANSendId                     = 70
    CANReceiveId                  = 71
    CANSecondaryId                = 72
    autoStartMode                 = 77
    protectionMode                = 81
    eepromCoordinateStore         = 84
    zeroUserVariables             = 85
    applicationStatus             = 128
    programCounter                = 130
    lastTmclError                 = 131
    tickTimer                     = 132
    randomNumber                  = 133
