from logging import INFO
import os
from fastapi import FastAPI, HTTPException, WebSocket, UploadFile, File
from websockets import ConnectionClosed
import uvicorn
import asyncio
import json

from api.schema import *
from api import API_LOGGER
from hal.statusManager.VaemManager import VaemStatus
from hal.VaemControl import VaemControl
from hal.SimVaemControl import SimVaemControl
from hal.ExceptionTypes import VaemNotAvailable
from typing import List
from config import vaem_config

description= """
## Software for controlling VAEM Festo valve control module

## Use Cases
* Read Status of the module
* Open/close up to 8 electromagnetic valves
"""
tags_metadata = [
    {
        "name": "status",
        "description": "Status reporting"
    },
    {
        "name": "Actions",
        "description": "Open and close valves"
    },    
    {
        "name": "Identification",
        "description": "This identifies te device"
    }, 
    {
        "name": "index",
    }, 
    {
        "name": "configuration",
        "description": "configure the device"
    }, 
]

api_ver = "v0.0.1"
_PORT = 5002
VaemHandler : VaemControl = None
StatusHandler : VaemStatus = None

vaem = FastAPI(title="VAEM Module", description=description, version=api_ver, openapi_tags=tags_metadata)

@vaem.on_event("startup")
async def startup_event():
    global StatusHandler
    global VaemHandler

    try:
        VaemHandler = VaemControl()
        #VaemHandler = SimVaemControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate liquid control module {e}')

    StatusHandler = VaemStatus(VaemHandler, timeout = 0.5, use_asyncio=True)
    t = asyncio.get_event_loop()
    t.create_task(StatusHandler.run())

@vaem.get("/", tags=["index"])
async def index():
    return {"visit": "/docs, /redoc"}


@vaem.get('/version_id', tags=['Identification'])
async def version():
    global VaemHandler
    return VaemHandler.version


@vaem.get('/who_am_i', tags=['Identification'], response_model=Who_Am_I)
async def who_am_i():
    global VaemHandler
    server_port = int(os.environ['APP_PORT'])
    identification = Who_Am_I(  name=VaemHandler.name, 
                                serverPort=server_port, 
                                hasGantryHeadOffset=True,
                                devices=[Device(id=VaemHandler.vaemConfig.index(x), serialNumber=x.ip) for x in VaemHandler.vaemConfig])

    return identification

@vaem.get("/status/", tags=['status'])
async def read_status():
    """
    The status of all the VAEM
    """
    global StatusHandler

    return Status(error_status=False, tool_status=str(StatusHandler.status))

@vaem.post('/configure_valves', tags=['Actions'], response_model=str)
async def configure_valves(  vaem_id:int,
                        valve1:int,
                        valve2:int,
                        valve3:int,
                        valve4:int,
                        valve5:int,
                        valve6:int,
                        valve7:int,
                        valve8:int):
    """An action to configure particilar valves as query params.
    A valve is selected if the openning time is different from 0
    """
    global VaemHandler
    global StatusHandler

    for i,v in enumerate([valve1, valve2, valve3, valve4, valve5, valve6, valve7, valve8]):
        await VaemHandler.configure_valve(vaem_id=vaem_id, valve_id=i, opening_time=v)
    
    return str(StatusHandler.status)

@vaem.post('/open_valves', tags=['Actions'], response_model=str)
async def open_valves(vaem_id:int):
    """An action to open particilar valves as query params.
    A valve is openned if the openning time is different from 0
    """
    global VaemHandler
    global StatusHandler

    await VaemHandler.open_valves(vaem_id=vaem_id)
    
    return str(StatusHandler.status)


@vaem.post('/clear_errors', tags=['Actions'], response_model=str)
async def clear_errors(vaem_id:int):
    """Clears all the errors if any on the vaem"""
    global VaemHandler
    global StatusHandler

    await VaemHandler.clear_valve_errors(vaem_id=vaem_id)

    return str(StatusHandler.status)

@vaem.post("/upload_config", tags=['configuration'])
async def create_upload_config_file(file: UploadFile = File(...)):
    global VaemHandler
    
    content = await file.read()
    try:
        content = content.decode('utf8').replace("'", '"')
        content_json = json.loads(content)
    except:
       raise HTTPException(status_code=415, detail="Invalid Input file or empty", headers={'Exception': 'Not compatible configuration'})

    try:
        vaem_config.set_config(content_json)
    except ValueError:
        raise HTTPException(status_code=422, detail="Cannot save file. Incopatible configuration", headers={'Exception': 'Not compatible configuration'})
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Cannot save file. Server error {e}", headers={'Exception': 'Internal server error'})
    API_LOGGER.info("New config loading...")

    VaemHandler.reload_config()
    API_LOGGER.info("New config reloaded")


@vaem.websocket("/status")
async def websocket_endpoint(websocket: WebSocket):
    global StatusHandler
    
    await websocket.accept()
    
    while True:
        status = StatusHandler.status
        try:
            await websocket.send_json(json.dumps(status))
        except ConnectionClosed:
            break
        await asyncio.sleep(1)


if __name__ == "__main__":
    try:
        server_port = int(os.environ['APP_PORT'])
    except:
        raise RuntimeError("\"APP_PORT\" environment variable must be set to the value of the port on which the server will be running")

    uvicorn.run("restApi:vaem", host="0.0.0.0", port=server_port, log_level=INFO, reload=True)

