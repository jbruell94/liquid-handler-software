from os import X_OK
from typing import List
from pydantic import BaseModel, Field

class ValveControl(BaseModel):
    valve_id: int = Field(
        title = 'ID of the valve to control',
        ge = 0,
        le = 7
    )
    opening_time: float = Field(
        title="Opening time for the corresponding valve",
        ge = 0,
        le = 2000
    )

class Device(BaseModel):
    id: int = Field(
        title='The id that is used to address this devices whithin that tool'
    )
    serialNumber: str = Field(
        title='The serial number of this concrete device'
    )

class Who_Am_I(BaseModel):
    name: str = Field(
        title = 'Name of the tool'
    )
    hasGantryHeadOffset: bool = Field(
        title="Whether the tool supports gantry offset"
    )    
    serverPort: int = Field(
        title='The port at which the tool server could be found'
    )
    devices: List[Device]=Field(
        title='The complete list of devices that are connected to the machine'
    )

class Status(BaseModel):
    error_status: bool=Field(
        title="The current error sttaus of the whole module. True if error is present"
    )
    tool_status: str=Field(
        title='The tool specific status. Here if a single veam is malfunctioning wiil be showed'
    )