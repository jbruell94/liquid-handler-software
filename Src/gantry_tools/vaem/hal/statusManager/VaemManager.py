import time
from typing import List
import asyncio

from hal.statusManager.StatusDataTypes import VaemStatusType
from hal import VAEM_LOG
from hal.VaemControl import VaemControl
from hal.SimVaemControl import SimVaemControl

class VaemStatus():
    """
    This is the context class that handles context switches and state methods
    """

    _vaem_inst: VaemControl = None
    _status : List[VaemStatusType] = []
    _timeout: int = 0.5
    _use_asyncio: bool = False

    def __init__(self, vaem_inst : VaemControl = None, timeout: int = 0.5, use_asyncio: bool = False):
        self._status = []
        self._vaem_inst = vaem_inst
        self._timeout = timeout
        self._use_asyncio = use_asyncio
        self._register_devices()
        
    def _register_devices(self):
        """register devices      
        """
        try:
            for i in range(0, len(self._vaem_inst.vaemModules)):
                self._status.append(VaemStatusType(id=i, valves=dict()))
        except Exception as e:
            VAEM_LOG.info(f'exception {e}')
            VAEM_LOG.error('Could not register pressure supply devices')

    @property
    def status(self):
        status_dict = {"VAEM status": list(map(lambda x: x.__dict__, self._status))}
        return status_dict

    async def update_status(self):
        if self._status is not None:
            for i in range(0, len(self._status)):
                self._status[i] = self._vaem_inst.get_status(id=self._status[i].id)
                
    async def run(self):
        while True:
            await self.update_status()
            
            if self._use_asyncio:
                await asyncio.sleep(self._timeout)
            else:
                time.sleep(self._timeout)


if __name__ == "__main__":
    async def func():
        valves = {x : {"opening_time" : 10} for x in range(0, 8)}
        lq = SimVaemControl()
        man = VaemStatus(lq)
        await lq.open_valve(valves)
        print(lq._valve_opening_times)
        while 1:
            await man.update_status()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())