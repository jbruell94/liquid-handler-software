from typing import Any, Dict, List
import asyncio
import time

from driver.drvVaem.vaem import vaemDriver
from hal import VAEM_LOG
import config.vaem_config as vaem_cfg
from hal.ExceptionTypes import VaemNotAvailable
from hal.statusManager.StatusDataTypes import VaemStatusType

_VERSION    = '0.0.1'
_NAME       = 'VAEM_TOOL'

class VaemControl():
    """This class contains all the interfaces 
    that are needed to control the liquid opperations
    This includes aspirating, dispensing, mixing, tip ejections etc."""
    _instance = None
    def __new__(cls):
        if cls._instance is None:
            VAEM_LOG.info(f"Initilized Liquid Control class. One instance allowed")
            cls._instance = super(VaemControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.vaemModules = None
        try:
            self.vaemConfig = vaem_cfg.get_config()
            self.vaemModules = list(map(lambda x: vaemDriver(x, VAEM_LOG), self.vaemConfig))
        except ConnectionError:
            VAEM_LOG.error("Failed to connect to valve control terminal")

    async def configure_valve(self, vaem_id: int, valve_id: int, opening_time: int):
        """
        Configure the valve openinng times one by onex. 
        This reflects selecting the valve. 
        If the opening time is 0 the valve is unselected

        @param: vaem_id - id of the vaem from connected devices
        @param: valve_id - the id of the valve to be configured
        @param: openning_time - the openning time of the selected valve. Must be 0 to unselect

        raises:
            ValueError - if one of the opening time is incorrect
            IndexError - if vaem id is incorrect
            VeamNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                try:
                    if opening_time != 0:
                        await self.vaemModules[vaem_id].select_valve(valve_id)
                        await self.vaemModules[vaem_id].configure_valves(valve_id, opening_time)
                    else:
                        await self.vaemModules[vaem_id].deselect_valve(valve_id)
                except ValueError as v: 
                    VAEM_LOG.error(f'Cannot configure valves {v}')
                    raise               

            else: raise IndexError('Wrong VAEM Index')
        else: raise VaemNotAvailable


    async def open_valves(self, vaem_id: int = 0):
        """
        Open the valves that have been configured with openning time different from 0
        
        @param: vaem_id - string 
        
        raises:
            IndexError - if vaem id is incorrect
            VeamNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                
                await self.vaemModules[vaem_id].open_valve()
                status = self.vaemModules[vaem_id].read_status()

                if status["Error"]:
                    pass
                    #TODO Set status in Liquid manager
                else:
                    pass
                    #TODO Set status in Liquid manager                
                #TODO Set valve status in Status manager
                #self._valveStatus[vaemDevice]["valves"] = valves
            else: raise IndexError('Wrong VAEM Index')
        else: raise VaemNotAvailable


    @property
    def version(self):
        return _VERSION

    @property
    def name(self):
        return _NAME
        
    async def clear_valve_errors(self, vaem_id: int):
        """
        Clears all the errors on a given VAEM

        @param: vaem_id -> string

        raises:
            IndexError - if the index of the vaem is wrong
            VaemNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                await self.vaemModules[vaem_id].clear_error()
            else: raise IndexError
        else: raise VaemNotAvailable

    def get_status(self, id: int):
        status: Dict[str, Any]

        status = self.vaemModules[id].read_status()
        status.pop('OperatingMode')

        return VaemStatusType(id=id, status=status.pop('Status'), 
        errorStatus=status.pop('Error'), readiness=status.pop('Readiness'),
        valves=status)
        
    def reload_config(self):
        VaemControl.__init__(self)


if __name__ == "__main__":
    async def func():
        lq = VaemControl()
        await lq.clear_valve_errors(0)
        while 1:
            print(lq.get_status(0))
            await lq.open_valve(0, 5, 400)
            print(lq.get_status(0))
            time.sleep(2)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())
