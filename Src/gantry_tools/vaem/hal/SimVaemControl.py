from typing import Any, Dict, List
import asyncio

from hal import VAEM_LOG
import config.vaem_config as vaem_cfg
from hal.ExceptionTypes import VaemNotAvailable
from hal.statusManager.StatusDataTypes import VaemStatusType

_VERSION    = '0.0.1'
_NAME       = 'VAEM_TOOL'
class SimVaemControl():
    """This class contains all the interfaces 
    that are needed to control the liquid opperations
    This includes aspirating, dispensing, mixing, tip ejections etc."""
    _instance = None
    def __new__(cls):
        if cls._instance is None:
            VAEM_LOG.info(f"Initilized Liquid Control class. One instance allowed")
            cls._instance = super(SimVaemControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.vaemModules = None
       
        try:
            self.vaemConfig = vaem_cfg.get_config()
            self.vaemModules = list(map(lambda x: list(map(lambda y: {'opening_time': 0}, range(8))), self.vaemConfig))
            VAEM_LOG.info(f'modules {self.vaemModules}')
        except ConnectionError:
            VAEM_LOG.error("Failed to connect to valve control terminal")

    @property
    def version(self):
        return _VERSION

    @property
    def name(self):
        return _NAME

    async def open_valve(self, vaem_id: int = 0, valve_id: int = 0, opening_time: float = 0.0):
        """
        Open the valves that have the parameter 'opening time' different from 0
        
        @param: vaem_id - string 
        
        raises:
            ValueError - if one of the opening time is incorrect
            IndexError - if vaem id is incorrect
            VeamNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                try:
                    if opening_time != 0:
                        self.vaemModules[vaem_id][valve_id]['opening_time'] = opening_time
                except ValueError as e: raise ValueError(f'Cannot configure valves {e}')                
            else: raise IndexError('Wrong VAEM Index')
        else: raise VaemNotAvailable


    async def clear_valve_errors(self, vaem_id: str):
        """
        Clears all the errors on a given VAEM

        @param: vaem_id -> string

        raises:
            IndexError - if the index of the vaem is wrong
            VaemNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                for _ in range(10):
                    await asyncio.sleep(0.1)
            else: raise IndexError
        else: raise VaemNotAvailable

    def get_status(self, id: int):
        status: Dict[str, Any] = {}

        status['Status'] = False
        status['Error'] = True
        status['Readiness'] = True
        status['OperatingMode'] = False

        for i in range(0, 8):
            status[str(i)] = self.vaemModules[id][i]

        status.pop('OperatingMode')
        return VaemStatusType(id=id, status=status.pop('Status'), 
        errorStatus=status.pop('Error'), readiness=status.pop('Readiness'),
        valves=status)
        
    def reload_config(self):
        SimVaemControl.__init__(self)

if __name__ == "__main__":
    async def func():
        valves = {x : {"opening_time" : 10} for x in range(0, 8)}
        lq = SimVaemControl()
        l = [{'id': lq.vaemConfig.index(x), 'serialNumber': x.ip} for x in lq.vaemConfig]


        await lq.open_valve(valves)
        print(lq.get_status(0))
        print(lq.get_status(1))


    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())