from enum import IntEnum
from pymodbus.client.sync import ModbusTcpClient as TcpClient
import logging

from driver.IAnalog import IAnalogModule
from driver.IDigital import IDigitalModule
from config.data_types import IoConfig

class AdamRegTypes:
    COIL_OUTPUTS_REGS       = 1
    COIL_INPUT_REGS         = 2
    HOLDING_REGS            = 3


class AdamChannels(IntEnum):
    channel_0   = 0    
    channel_1   = 1
    channel_2   = 2
    channel_3   = 3
    channel_4   = 4
    channel_5   = 5
    channel_6   = 6
    channel_7   = 7
    channel_avg = 8

class AdamStartAddressInputRegs(IntEnum):
    digitalIn       = 0
    digitalOut      = 16
    resetMax        = 101
    resetMin        = 111
    highAlarm       = 131
    lowAlarm        = 141

class AdamStartAddressHoldRegs(IntEnum):
    analogIn        = 0
    analogOut       = 10
    histMin         = 33
    moduleName1     = 211
    moduleName2     = 212
    channelEn       = 221

class AdamDevices():
    Adam6017 = {
        'name' : '6017',
        'anInputs' : 8,
        'anOutputs' : 0,
        'digInputs' : 0,
        'digOutputs' : 2 
    }
    Adam6052 = {
        'name' : '6052',
        'anInputs' : 0,
        'anOutputs' : 0,
        'digInputs' : 8,
        'digOutputs' : 8 
    }
    Adam6024 = {
        'name' : '6024',
        'anInputs' : 6,
        'anOutputs' : 2,
        'digInputs' : 2,
        'digOutputs' : 2 
    }

AdamModulesDict = {
    "Adam6017" : AdamDevices.Adam6017,
    "Adam6052" : AdamDevices.Adam6052,
    "Adam6024" : AdamDevices.Adam6024,
}


def getModbusAddress(startAddr, channel):
    return (startAddr + channel)

class adamDriver(IAnalogModule, IDigitalModule):
    def __init__(self, config: IoConfig, log: logging, connect: bool = True):
        self.config = config
        self.module = AdamModulesDict[self.config.module]
        self.log = log

        if connect:
            self.client = TcpClient(host=self.config.ip, port=self.config.port, timeout=3)   
            for i in range(5):         
                if self.client.connect():
                    break
                else:
                    self.log.warning(f"trying to reconnect to {self.config.module}")
                if i == 4:
                    raise ConnectionError
                
            self.log.info(f'Connected to module : {self.config.module}')


    def _readRegister(self, address, num_bytes, type=AdamRegTypes.HOLDING_REGS):
        try:
            if type == AdamRegTypes.COIL_INPUT_REGS:
                data = self.client.read_discrete_inputs(address, num_bytes, unit=self.config.modbus_slave)
                return data.bits
            elif type == AdamRegTypes.COIL_OUTPUTS_REGS:
                data = self.client.read_coils(address, num_bytes, unit=self.config.modbus_slave)
                return data.bits
            elif type == AdamRegTypes.HOLDING_REGS:
                data = self.client.read_holding_registers(address, num_bytes, unit=self.config.modbus_slave)   
                return data.registers     
            else:
                self.log.info('Must select a type') 
            
        except Exception as e:
            self.log.error(f"Error while reading: {e} with {self.config.module}")
            raise


    def _writeRegiter(self, address, data, type=AdamRegTypes.HOLDING_REGS):
        if data < 0:
            data = data + 2**16
        try:
            if type == AdamRegTypes.COIL_OUTPUTS_REGS:
                self.client.write_coil(address, data, unit=self.config.modbus_slave)
            elif type == AdamRegTypes.HOLDING_REGS:
                self.client.write_register(address, value=data, unit=self.config.modbus_slave)
            else:
                self.log.warn(f'Must choose a type of register to write to')
        except Exception as e:
            self.log.error(f"error while writing: {e} with {self.config.module}")
            raise

    # def readModuleName(self):
    #     name = {}

    #     name['name_1'] = self._readRegister(getModbusAddress(AdamStartAddressHoldRegs.moduleName1, 0))
    #     name['name_2'] = self._readRegister(getModbusAddress(AdamStartAddressHoldRegs.moduleName2, 0))

    #     return name

    def readAllAnalog(self):
        anVals = []

        for i in range(self.module['anInputs']):
            anVals.append(self.readAnalogChannel(i, 'in'))

        return anVals

    def writeAllAnalog(self, analogOut):        
        for i in range(self.module['anOutputs']):
            self.writeAnalogChannel(i, analogOut[i])

    def readAnalogChannel(self, channel, type):
        """
        channel -> channel to read
        type -> "out", "in"
        """
        #some more checks are needed for channel availability
        if type == "in":
            if self.module['anInputs'] == 0 or channel < 0 or channel >= self.module['anInputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            analogVals = self._readRegister( \
                getModbusAddress(AdamStartAddressHoldRegs.analogIn, AdamChannels.channel_0 + channel), num_bytes=1)
        elif type == "out":
            if self.module['anOutputs'] == 0 or channel < 0 or channel >= self.module['anOutputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            analogVals = self._readRegister( \
                getModbusAddress(AdamStartAddressHoldRegs.analogOut, AdamChannels.channel_0 + channel), num_bytes=1)
        
        return analogVals[0]

    def writeAnalogChannel(self, channel, analogOut):
        if self.module['anOutputs'] == 0 or channel < 0 or channel >= self.module['anOutputs']:
            raise ValueError("Invalid channel number for this ADAM device")
        
        self._writeRegiter(getModbusAddress(AdamStartAddressHoldRegs.analogOut, AdamChannels.channel_0+channel), analogOut)

    def readAllDigital(self, type="out"):
        if type not in {"in", "out"}:
            raise ValueError('"type" must be either "in" or "out"')

        if type == "out":
            digital = self._readRegister( \
                getModbusAddress(AdamStartAddressInputRegs.digitalOut, AdamChannels.channel_0), num_bytes=self.module['digOutputs'], type=AdamRegTypes.COIL_OUTPUTS_REGS)
        else:
            digital = self._readRegister( \
                getModbusAddress(AdamStartAddressInputRegs.digitalIn, AdamChannels.channel_0), num_bytes=self.module['digInputs'], type=AdamRegTypes.COIL_INPUT_REGS)
        return digital

    def writeAllDigital(self, digitalOut):
        for i in range(self.module['digOutputs']):
            self.writeDigitalChannel(i, digitalOut[i])

    def readDigitalChannel(self, channel, type="out"):
        """
        channel -> channel to read
        type -> "out", "in"
        """
        if type not in {"in", "out"}:
            raise ValueError('"type" can be either "in" or "out"')

        if type == "out":
            if self.module['digOutputs'] == 0 or channel < 0 or channel >= self.module['digOutputs']:
                raise ValueError("Invalid channel number for this ADAM device")
        else:
            if self.module['digInputs'] == 0 or channel < 0 or channel >= self.module['digInputs']:
                raise ValueError("Invalid channel number for this ADAM device")

        result = self.readAllDigital(type)
        
        return result[channel]

    def writeDigitalChannel(self, channel, digitalOut):
        if self.module['digOutputs'] == 0 or channel < 0 or channel >= self.module['digOutputs']:
            raise ValueError("Invalid channel number for this ADAM device")
        
        #as per doc 0xff00 sets the coil, 0x0000 resets it
        valueToWrite = 0 if digitalOut == 0 else 0xff00
        self._writeRegiter(getModbusAddress(AdamStartAddressInputRegs.digitalOut, AdamChannels.channel_0+channel), valueToWrite, type=AdamRegTypes.COIL_OUTPUTS_REGS)

    def convertIncrementsToVoltage(self, increments: int, source: str):
        if source not in {"adc", "dac"}:
            raise ValueError('"source" must be "adc" or "dac"')
        
        if source == "adc":
            range = (self.config.adc_voltage_range[0], self.config.adc_voltage_range[1])
            resolution = self.config.adc_resolution_bits
        else:
            range = (self.config.dac_voltage_range[0], self.config.dac_voltage_range[1])
            resolution = self.config.dac_resolution_bits

        return range[0] + increments * ((range[1] - range[0]) / ((1 << resolution) - 1))

    def convertVoltageToIncrements(self, voltage: float):
        return round(voltage * (((1 << self.config.dac_resolution_bits) - 1) / \
                    (self.config.dac_voltage_range[1] - self.config.dac_voltage_range[0]) ))

"""
if __name__ == "__main__":

    with open('/home/pi/Liquid-Handler/liquid-handler-software/config/LiquidHandlerFileConfig.json') as jf:
        jData = json.load(jf)

    adamConfigs = jData["adamModules"]
    
    devices = {}
    for config in adamConfigs:
        devices[config] = adamDriver(adamConfigs[config])

    sensorConfig = jData["baumerSensors"]
    teachChannel = sensorConfig["0"]["teachingDevice"]["channel"]
    teachDevID = sensorConfig["0"]["teachingDevice"]["deviceID"]
    teachDev = devices[teachDevID]
    readChannel = sensorConfig["2"]["readingDevice"]["channel"]
    readDevID = sensorConfig["2"]["readingDevice"]["deviceID"]
    readDev = devices[readDevID]

    #teachSensor(teachDev, teachChannel, sensitivityLevelsDict[sensorConfig["sensor0"]["sensitivityLevel"]])


    while 1:
        analog_channel = readDev.readAnalogChannel(readChannel)
        voltage = readDev.convertAdcToVoltage(analog_channel)
        print(voltage)

        #print(readDev.convertVoltageToDistance(voltage))
        devices["adamDevice1"].writeDigitalChannel(1, 1)
        devices["adamDevice1"].writeDigitalChannel(2, 1)
        devices["adamDevice1"].writeDigitalChannel(3, 1)
        devices["adamDevice1"].writeDigitalChannel(4, 1)
        devices["adamDevice1"].writeDigitalChannel(5, 1)
        devices["adamDevice1"].writeDigitalChannel(6, 1)
        devices["adamDevice1"].writeDigitalChannel(7, 1)
        devices["adamDevice1"].writeDigitalChannel(8, 1)

        devices["adamDevice2"].writeAnalogChannel(1, 3)
        devices["adamDevice2"].writeAnalogChannel(2, 6)
        print(devices["adamDevice2"].readAnalogChannel(0, "out"))
        print(devices["adamDevice2"].readAnalogChannel(1, "out"))

        time.sleep(2)
"""