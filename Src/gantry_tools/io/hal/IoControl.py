import asyncio
from typing import List

#TODO define custon exceptions
from hal.ExceptionTypes import IoNotAvailableError
from hal import IO_LOG
from driver.adam import adamDriver
from config import io_config

class IOControl():
    """HAL class that controls input output devices,
    such as digital inputs/outputs and analog inputs/outputs
    """
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            IO_LOG.info(f"Initilized IO Control class. One instance allowed")
            cls._instance = super(IOControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self._adamModules : List[adamDriver] = None
        try:
            self._adamConfig = io_config.get_config()
            self._adamModules = list(map(lambda x: adamDriver(x, IO_LOG), self._adamConfig))
        except ConnectionError:
            IO_LOG.error('Could not connect to one or all Adams. Please check the configuration or cabling!')
            raise IoNotAvailableError
    
    def _find_io_module(self, io_id: int):
        try:
            return list(filter(lambda x: x.config.id == io_id, self._adamModules)).pop()
        except IndexError:
            raise ValueError("No such IO module")

    async def set_analog_output_voltage(self, adam_id: int, adam_channel: int, voltage: float):
        adam = self._find_io_module(adam_id)
        adc_value = adam.convertVoltageToIncrements(voltage)
        adam.writeAnalogChannel(adam_channel, adc_value)

    async def get_analog_voltage(self, adam_id: int, adam_channel: int, type: str) -> float:
        """
        Get the voltage on the analog input or output for the selected channel
        "type" should be either "in" or "out" for reading inputs or outputs, respectively
        """

        adam = self._find_io_module(adam_id)
        adc_value = adam.readAnalogChannel(adam_channel, type)
        return adam.convertIncrementsToVoltage(adc_value, "adc" if type == "in" else "dac")

    async def set_digital_out(self, adam_id: int, adam_channel: int, state: int):
        """
        Set the required digital channels.
        channel -> bit mask for desired channels
        state -> state of the required channels
        e.g. channel = 3, state = 1 => channel 1 is set to 1 and channel 2 is set to 0
        """
        adam = self._find_io_module(adam_id)
        adam.writeDigitalChannel(adam_channel, state)

    async def get_digital(self, adam_id: int, adam_channel: int, type: str) -> int:
        """
        Get the state of the digital input or output for the selected channel
        "type" should be either "in" or "out" for reading inputs or outputs, respectively
        """
        adam = self._find_io_module(adam_id)
        return adam.readDigitalChannel(adam_channel, type)

    def reload_config(self):
        IOControl.__init__(self)


"""
    def LedSetColor(self, opperation:str):

        try:
            if opperation == 'Idle':                
                self.LedHandler.ledStripSet(LedControl.COLORS.FestoColor)
            elif opperation == "Running":
                self.LedHandler.ledStripSet(LedControl.COLORS.RandomColor)
            elif opperation == "Error":
                self.LedHandler.ledStripSet(LedControl.COLORS.ErrorColor)
            else:
                self.LedHandler.ledStripSet(LedControl.COLORS.WhiteColor)
        except Exception as e:
            print(f"Setting Led strip failed {e}")
"""

if __name__ == "__main__":
    async def func():
        io = IOControl()
        io.set_digital_out(255, 255)
        print(bin(io.get_digital_out()))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())


