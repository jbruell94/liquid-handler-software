from dataclasses import dataclass
from typing import List

@dataclass
class DigitalStatus():
    inputs : List[int] = None
    outputs : List[int] = None

@dataclass
class AnalogStatus():
    inputs : List[float] = None
    outputs : List[float] = None