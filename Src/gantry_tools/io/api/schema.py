from pydantic import BaseModel, Field

#I/O Control
class DigitalControl(BaseModel):
    digital_value: int = Field(
        title='Digital control',
        description='control digital outputs -> 0 turn output OFF, 1 - ON',
        le=1,
        ge=0
    )

class AnalogControl(BaseModel):
    voltage: float = Field(
        title='Analog Input/Output control',
        description='Reads or writes value to the ADC/DAC respectively in volts',
        ge=0
    )

