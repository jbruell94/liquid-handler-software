from logging import INFO
from fastapi import FastAPI, HTTPException, WebSocket, File, UploadFile
from websockets import ConnectionClosed
import uvicorn
import asyncio
import json

from api.schema import *
from api import API_LOGGER
from hal.IoControl import IOControl
from hal.SimIoControl import SimIOControl
from hal.statusManager.IoControlManager import IOControlStatus
from config import io_config

description= """
## Software for usage of IO tool module

## Use Cases
* Read Analog Input data
* Set Analog Output data
* Read Digital Input/Output data
* Set Digital Output data
"""
tags_metadata = [
    {
        "name": "input/output",
        "description": "Operations with input/ouput devices"
    }  
]

api_ver = "v1.1.0"
StatusHandler : IOControlStatus = None
IoHandler : IOControl = None

io = FastAPI(title="IO Module", description=description, version=api_ver, openapi_tags=tags_metadata)

@io.on_event("startup")
async def startup_event():
    global StatusHandler
    global IoHandler

    try:
        IoHandler = IOControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate IO module {e}')
        raise

    StatusHandler = IOControlStatus(IoHandler, use_asyncio=True)
    t = asyncio.get_event_loop()
    t.create_task(StatusHandler.run())

@io.get("/")
async def index():
    return {"visit": "/docs, /redoc"}

@io.get("/status/", tags=['status'])
async def read_status():
    """
    The status of the IO module
    """
    global StatusHandler
    s = []

    s.append({"Analog": StatusHandler.status_analog})
    s.append({"Digital": StatusHandler.status_digital})
    return s

@io.post('/digital_out/{module_id}/{digital_ch}', tags=['output'], response_model=DigitalControl)
async def write_digital_output(digitalOuts: DigitalControl, module_id: int, digital_ch: int):
    """
    Set the digital output state
    The request is sent as a combination of bitmask and request.

    * module_id -> ID of the IO module
    * digital_ch -> digital output channel of the selected module
    * digital_value -> state of output (0 - OFF or 1 - ON)
    """
    global IoHandler
    if module_id is not None and digital_ch is not None:
        try:
            await IoHandler.set_digital_out(module_id, digital_ch, digitalOuts.digital_value)
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")

    return {"digital_value" : await IoHandler.get_digital(module_id, digital_ch, "out")}

@io.get('/digital_out/{module_id}/{digital_ch}', tags=['output'], response_model=DigitalControl)
async def read_digital_output(module_id: int, digital_ch: int):
    """
    Get the digital output state
    * module_id -> ID of the IO module
    * digital_ch -> digital output channel of the selected module
    * digital_value -> state of output (0 - OFF or 1 - ON)
    """

    global IoHandler
    if module_id is not None and digital_ch is not None:
        try:
            return {"digital_value" : await IoHandler.get_digital(module_id, digital_ch, "out")}
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")

@io.get('/digital_in/{module_id}/{digital_ch}', tags=['input'], response_model=DigitalControl)
async def read_digital_input(module_id: int, digital_ch: int):
    """
    Get the digital in state
    * module_id -> ID of the IO module
    * digital_ch -> digital output channel of the selected module
    * digital_value -> state of input (0 - OFF or 1 - ON)
    """

    global IoHandler
    if module_id is not None and digital_ch is not None:
        try:
            return {"digital_value" : await IoHandler.get_digital(module_id, digital_ch, "in")}
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")

@io.post('/analog_out/{module_id}/{analog_ch}', tags=['output'])
async def write_analog_output(analogOuts: AnalogControl, module_id: int, analog_ch: int):
    """
    Set the value for analog output in volts
    * module_id -> ID of the IO module
    * analog_ch -> analog output channel of the selected module
    * voltage -> state of output in volts
    """
    global IoHandler
    if module_id is not None and analog_ch is not None:
        try:
            await IoHandler.set_analog_output_voltage(module_id, analog_ch, analogOuts.voltage)
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")
    return {"voltage" : await IoHandler.get_analog_voltage(module_id, analog_ch, "out")}

@io.get('/analog_out/{module_id}/{analog_ch}', tags=['output'], response_model=AnalogControl)
async def read_analog_output(module_id: int, analog_ch: int):
    """
    Get the value on analog output in volts
    * module_id -> ID of the IO module
    * analog_ch -> analog output channel of the selected module
    """
    global IoHandler
    if module_id is not None and analog_ch is not None:
        try:
            return {"voltage" : await IoHandler.get_analog_voltage(module_id, analog_ch, "out")}
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")


@io.get('/analog_in/{module_id}/{analog_ch}', tags=['input'], response_model=AnalogControl)
async def read_analog_input(module_id: int, analog_ch: int):
    """
    Get the value on analog input in volts
    * module_id -> ID of the IO module
    * analog_ch -> analog output channel of the selected module
    """
    global IoHandler
    result: AnalogControl = {}
    if module_id is not None and analog_ch is not None:
        try:
            result['voltage'] = await IoHandler.get_analog_voltage(module_id, analog_ch, "in")
        except ValueError as e:
            raise HTTPException(status_code=422, detail=str(e))
    else:
        raise HTTPException(status_code=404, detail="No such ID and/or channel")
    
    return result

@io.post("/upload_config", tags=['configuration'])
async def create_upload_config_file(file: UploadFile = File(...)):
    global IoHandler
    
    content = await file.read()
    try:
        temp = content.decode('utf8').replace("'", '"')
        recvJson = json.loads(temp)
    except:
       raise HTTPException(status_code=415, detail="Invalid Input file or empty", headers={'Exception': 'Not compatible configuration'})

    try:
        io_config.set_config(recvJson)
    except ValueError:
        raise HTTPException(status_code=422, detail="Cannot save file. Incopatible configuration", headers={'Exception': 'Not compatible configuration'})
    except Exception:
        raise HTTPException(status_code=500, detail="Cannot save file. Server error", headers={'Exception': 'Internal server error'})
    
    IoHandler.reload_config()
    API_LOGGER.info("New config reloaded")


@io.websocket("/status")
async def websocket_endpoint(websocket: WebSocket):
    global StatusHandler
    status = []
    
    await websocket.accept()
    
    while True:
        status.append({"Analog": StatusHandler.status_analog})
        status.append({"Digital": StatusHandler.status_digital})
        try:
            await websocket.send_json(json.dumps(status))
        except ConnectionClosed:
            break
        await asyncio.sleep(1)


if __name__ == "__main__":
    uvicorn.run("restApi:io", host="0.0.0.0", port=5003, log_level=INFO, reload=True)

