from config.data_types import IoConfig
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Dict, Any, List
from config import ADAM_CONFIG_FILE, CONFIG_LOG
import json
import pkg_resources
from jsonschema import validate, ValidationError, draft202012_format_checker

class IoEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, IoConfig):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

DEF_ADAMS = [{}, {}, {}]
DEF_IP_ADAM = '192.168.0.111'
DEF_PORT_ADAM = 502
DEF_NAME_ADAM = 'Adam6017'
DEF_SLAVE_ID_ADAM = 1
DEF_ADC_VOLTAGE_RANGE = [-10, 10] #volts
DEF_DAC_VOLTAGE_RANGE = [0, 10] #volts
DEF_ADC_RESOLUTION_BITS = 16 #bits
DEF_DAC_RESOLUTION_BITS = 12 #bits

def _build_adam_config(adamJson: Dict[str, Any]) -> List[IoConfig]:
    config: Dict[str, Any]
    adamsConfig = []
    used_ids = set()
    counter = 0

    for config in adamJson if adamJson else DEF_ADAMS:
        if 'id' not in config:
            config['id'] = counter

        if config['id'] not in used_ids:
            used_ids.add(config['id'])
            adam = IoConfig(id=config['id'],
                                ip=config.get('ip', DEF_IP_ADAM),
                                port=config.get('port', DEF_PORT_ADAM),
                                module=config.get('module', DEF_NAME_ADAM),
                                modbus_slave=config.get('modbus_slave', DEF_SLAVE_ID_ADAM),
                                adc_voltage_range=config.get('adc_voltage_range', DEF_ADC_VOLTAGE_RANGE),
                                adc_resolution_bits=config.get('adc_resolution_bits', DEF_ADC_RESOLUTION_BITS),
                                dac_voltage_range=config.get('dac_voltage_range', DEF_DAC_VOLTAGE_RANGE),
                                dac_resolution_bits=config.get('dac_resolution_bits', DEF_DAC_RESOLUTION_BITS)
            )
            adamsConfig.append(adam)
            counter += 1
        else:
            raise ValueError("IDs should be unique")

    return adamsConfig

def _write_config(configs: List, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config

def get_config() -> List[IoConfig]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(ADAM_CONFIG_FILE)
    config = _build_adam_config(json_config)
    _write_config(config, ADAM_CONFIG_FILE, IoEncoder)
    return config

def set_config(io_cfg: List[IoConfig]):
    """
    Writes the desired configuration to the config file
    """
    
    try:
        schema = pkg_resources.resource_string('config', 'io_schema.json').decode('utf8')
    except FileNotFoundError:
        CONFIG_LOG.critical("Schema resource could not be loaded")
        raise

    try:
        schema_json = json.loads(schema)
        validate(io_cfg, schema_json, format_checker=draft202012_format_checker)
        CONFIG_LOG.info(f'Writing file {io_cfg}')    
        _write_config(io_cfg, ADAM_CONFIG_FILE, IoEncoder)
    except ValidationError as e:
        CONFIG_LOG.error(f"Incompatible configuration: {e}")
        raise ValueError


if __name__ == "__main__":
    adamJson = _load_config(ADAM_CONFIG_FILE)
    adam = _build_adam_config(adamJson)
    _write_config(adam, ADAM_CONFIG_FILE, IoEncoder)
