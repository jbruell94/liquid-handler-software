from pydantic import BaseModel, Field
from typing import List
import uuid

class Device(BaseModel):
    id: int = Field(
        title="A unique ID used to refer to a particular PGVA"
    )

    serialNumber: str = Field(
        title="Serial number of the device"
    )

class Dimensions(BaseModel):
    xDimension: float
    yDimension: float
    zDimension: float

class ActionParams(BaseModel):
    name: str = Field(description="Name of the parameter")
    type: str = Field(description="Type of the parameter (string, float, etc.)")
    show: bool

class Action(BaseModel):
    id: object = Field(description="ID of the action", alias="_id")
    name: str = Field(description="Name of the action")
    description: str = Field(description="Short info about the action")
    parameters: List[ActionParams] = Field(description="Parameters for that action")

class WhoAmI(BaseModel):
    id: object = Field(description="ID of the tool", alias="_id")

    name: str = Field(
        title="The name of the particular tool"
    )

    location: int = Field(
        title="The port on which the tool can be accessed"
    )

    dimensions: Dimensions = Field(
        title="The physical dimensions of the tool"
    )

    hasGantryHeadOffset: bool = Field(
        title="Whether the tool supports gantry offset"
    )
    
    referencePoint: str = Field(
        title="The reference point of the tool for mounting"
    )

    hardwareDevices: List[Device] = Field(
        title="A list of available devices on this tool"
    )

    actions: List[Action] = Field(
        title="A list of the supported actions by the tool"
    )

class Status(BaseModel):
    error_status: bool = Field(
        title="Boolean value to identify whether the tool has errors or not"
    )

    tool_status: str = Field(
        title="Detailed description of the error encountered by the tool"
    )