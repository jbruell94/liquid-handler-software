from logging import INFO
from fastapi import FastAPI, HTTPException, WebSocket, WebSocketDisconnect, UploadFile, File
import uvicorn
import asyncio
import json
import os

from api.schema import *
from api import API_LOGGER
from hal.pgva_control import PgvaControl
from hal.pgva_control_sim import SimPgvaControl
from hal.statusManager.pgvaManager import PgvaControlStatus
from hal.actions import get_actions
from config import pgva_config

description= """
## Software for PGVA tool

## Use Cases
* Set pressure/vacuum output
* Aspirate
* Dispense

Note: A DHOE must be connected to the PGVA tool in order to 
perform aspirate and dispense functions
"""
tags_metadata = [
    {
        "name": "Status",
        "description": "Status reporting"
    },
    {
        "name": "Actions",
        "description": "Specific actions that can be performed by the tool"
    },
    {
        "name": "Identification",
        "description": "Identification of the tool and devices connected to it"
    },
]

api_ver = "v0.0.1"
PgvaHandler : PgvaControl = None
StatusHandler: PgvaControlStatus = None

pgva = FastAPI(title="PGVA Handling Tool", description=description, version=api_ver, openapi_tags=tags_metadata)

@pgva.on_event("startup")
async def startup_event():
    global StatusHandler
    global PgvaHandler
    t = asyncio.get_event_loop()
    try:
        PgvaHandler = PgvaControl()
        #PgvaHandler = SimPgvaControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate liquid control module {e}')

    StatusHandler = PgvaControlStatus(PgvaHandler, timeout=1, use_asyncio=True)
    t.create_task(StatusHandler.run())

@pgva.get("/")
async def index():
    return {"visit": "/docs, /redoc"}

@pgva.get("/version_id", tags=['Identification'])
async def get_version():
    """
    Reads the version of the tool driver
    """

    global PgvaHandler
    
    return PgvaHandler.version

@pgva.get("/who_am_i", tags=['Identification'], response_model=WhoAmI)
async def who_am_i():
    """
    Request for tool discovery and identification
    """

    global PgvaHandler
    
    return WhoAmI(_id=PgvaHandler.id,
                    name=PgvaHandler.name,
                    location=os.getenv('APP_PORT'),
                    dimensions=PgvaHandler.dimensions,
                    referencePoint=PgvaHandler.referencePoint,
                    hasGantryHeadOffset = True,
                    hardwareDevices=PgvaHandler.get_available_devices(),
                    actions=get_actions())

@pgva.get("/status/", tags=['Status'], response_model=Status)
async def read_status():
    """
    The status of all the components of the LHM
    """
    global PgvaHandler

    return Status(error_status=PgvaHandler.error_status, tool_status=PgvaHandler.tool_status)

@pgva.post("/acknowledge_error/", tags=['Status'])
async def read_status():
    """
    Acknowledge and clear previously encountered error
    """
    global PgvaHandler

    PgvaHandler.acknowledge_error()


@pgva.post("/set_vacuum/{pgva_id}", tags=['Actions'])
async def set_vacuum(pgva_id: int, vacuum: float):
    """
    """
    global PgvaHandler
    global StatusHandler

    try:
        #TODO Get the names from the config files...
        await PgvaHandler.set_vacuum(vacuum, pgva_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Could not set vacuum: {e}", headers={"Exception": "No compatible name"})


@pgva.post("/set_pressure/{pgva_id}", tags=['Actions'])
async def set_pressure(pgva_id: int, pressure: float):
    """
    """
    global PgvaHandler
    global StatusHandler

    try:
        #TODO Get the names from the config files...
        await PgvaHandler.set_pressure(pressure, pgva_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Could not set pressure: {e}", headers={"Exception": "No compatible name"})

@pgva.post("/aspirate/{pgva_id}", tags=['Actions'])
async def aspirate(pgva_id: int, volume: float):
    global PgvaHandler
    try:
        await PgvaHandler.aspirate(volume, pgva_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Could not aspirate: {e}", headers={"Exception": "No compatible name"})

@pgva.post("/dispense/{pgva_id}", tags=['Actions'])
async def dispense(pgva_id: int, volume: float):
    global PgvaHandler
    try:
        await PgvaHandler.dispense(volume, pgva_id)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Could not dispense: {e}", headers={"Exception": "No compatible name"})

@pgva.post("/pick_up_tip/{pgva_id}", tags=['Actions'])
async def pick_up_tip(pgva_id: int):
    pass

@pgva.post("/drop_tip/{pgva_id}", tags=['Actions'])
async def drop_tip(pgva_id: int):
    pass


@pgva.post("/upload_config", tags=['configuration'])
async def create_upload_config_file(file: UploadFile = File(...)):
    global PgvaHandler
    
    content = await file.read()
    try:
        temp = content.decode('utf8').replace("'", '"')
        recvJson = json.loads(temp)
    except:
       raise HTTPException(status_code=415, detail="Invalid Input file or empty", headers={'Exception': 'Not compatible configuration'})

    try:
        pgva_config.set_config(recvJson)
    except ValueError:
        raise HTTPException(status_code=422, detail="Cannot save file. Incopatible configuration", headers={'Exception': 'Not compatible configuration'})
    except Exception:
        raise HTTPException(status_code=500, detail="Cannot save file. Server error", headers={'Exception': 'Internal server error'})
    
    await PgvaHandler.reload_config()
    API_LOGGER.info("New config reloaded")

@pgva.websocket("/status")
async def websocket_endpoint(websocket: WebSocket):
    global PgvaHandler
    global StatusHandler
    
    await websocket.accept()
    
    while True:
        status = StatusHandler.status_pg
        try:
            await websocket.send_json(json.dumps(status))
        except WebSocketDisconnect:
            break
        await asyncio.sleep(1)
    

if __name__ == "__main__":
    try:
        if os.getenv('APP_PORT'):
            server_port = int(os.environ['APP_PORT'])
        else:
            os.environ["APP_PORT"] = "5003"
            server_port = int(os.environ['APP_PORT'])
    except:
        raise RuntimeError("\"APP_PORT\" environment variable must be set to the value of the port on which the server will be running")

    uvicorn.run("restApi:pgva", host="0.0.0.0", port=server_port, log_level=INFO, reload=True)

