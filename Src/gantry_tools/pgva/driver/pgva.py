import logging
from pymodbus.client.sync import ModbusTcpClient as TcpClient
from pymodbus.client.sync import ModbusSerialClient as SerialClient
import asyncio

from config.data_types import Pgva as pgvaConfig

from common.ExceptionTypes import PressureRegulationFailed

class _ModbusCommands:    
#input registers
    VacuumActual=256
    PressureActual=257
    OutputPressureActual=258
    FirmwareVer=259
    FirmwareSubVer=260
    FirmwareBuild=261
    StatusWord=262
    DispenseValveOpenTimeH=263
    DispenseValveOpenTimeL=264
    PumpLifeCntrH=265
    PumpLifeCntrL=266
    PGVALifeCntrH=267
    PGVALifeCntrL=268
    VacuumActualmBar=269
    PressureActualmBar=270
    OutputPressureActualmBar=271
    MinDacIncr=272
    ZeroDacIncr=273
    MaxDacIncr=274
    LastModbusErr=275
    Seed=276
    ManMode=277
#holding registers
    ValveActuationTime=4096
    VacuumThreshold=4097
    PressureThreshold=4098
    OutputPressure=4099
    MinCalibrSetP=4100
    ZeroCalibrSetP=4101
    MaxCalibrSetP=4102
    TcpPort=4108
    UnitId=4109
    VacuumThresholdmBar=4110
    PressureThresholdmBar=4111
    OutputPressuremBar=4112
    ManualTrigger=4113
    DhcpSelect=4115
    StoreEeprom=4196
    ResetPumpLifeCntr=4197
    StartPreCalibration=4198
    AdjustCalibrSetPoints=4199
    AuthStart=4296
    KeyWrite=4297
    ConfigOngoing=4298
#multiple holding registers
    IpAddressMSH=12288
    IpAddressLSH=12289
    GWAdressMSH=12290    
    GWAdressLSH=12291    
    NetMaskMSH=12292
    NetMaskLSH=12293
    MacAddressA=12294
    MacAddressB=12295
    MacAddressC=12296
    ProductionYear=12297
    ProductionDate=12298
    ProductionNumber=12299

class PgvaDriver:
    def __init__(self, pgvaConfig: pgvaConfig, logger: logging, connect: bool = True):
        self.sensorData = {'vaccumChamber' : 0, 'pressureChamber': 0, 'outputPressure': 0}
        self.pgvaConfig = pgvaConfig
        self.log = logger
        
        if connect:
            if pgvaConfig.interface == 'serial':
                self.client = SerialClient(method="ascii", port=pgvaConfig.comPort, baudrate=pgvaConfig.baudrate)
            elif pgvaConfig.interface == "tcp/ip":
                self.client = TcpClient(host=pgvaConfig.ip, port=pgvaConfig.tcpPort)
            
            for _ in range(5):
                if self.client.connect():
                    break
                else:
                    self.log.warning(f'Failed to connect PGVA. Reconnecting attempt:, attempt {_}')
                if _ == 4: 
                    self.log.error(f'Failed to connect to PGVA {pgvaConfig}')
                    raise ConnectionError(f'Could not connect to Pgva {pgvaConfig}')
            
            self.log.info(f'Connected to PGVA {pgvaConfig}')

    def _readData(self, register, sign=True):
        data = 0
        try:
            data = self.client.read_input_registers(register, 1, unit=self.pgvaConfig.modbusSlave)
        except Exception as e:
            self.log("Error while reading : ", str(e))

        result = data.registers[0]

        if (sign):
            if (result & 0x8000):
                result = -(~result & 0xFFFF) - 1

        return result

    def _writeData(self, register, val, sign=True):
        status = object
        #print(f"{register}, {val}")
        if (sign):
            val &= 0xFFFF

        try:
            if val < 0:
                val = val + 2**16
            self.client.write_register(register, val, unit=self.pgvaConfig.modbusSlave)
            status = self.client.read_input_registers(_ModbusCommands.StatusWord, count=1, unit=self.pgvaConfig.modbusSlave) 
            while (status.registers[0] & 1) == 1:
                status = self.client.read_input_registers(_ModbusCommands.StatusWord,count=1, unit=self.pgvaConfig.modbusSlave)
        except Exception as e:
            self.log("error while writing : ", str(e)) 

    #calibration procedure for etting exact levels of P&V
    def _calibration(self):
        #send start calibartion
        self._writeData(_ModbusCommands.StartPreCalibration, 1, sign=True)
        
        #Set max pressure
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 2, sign=False)
        
        #enter actual max
        self._writeData(_ModbusCommands.MaxCalibrSetP, 450, sign=True)

        #Set zero pressure
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 1, sign=False)

        #enter actual 0
        self._writeData(_ModbusCommands.ZeroCalibrSetP, 0, sign=True)
               
        #Set min pressure 
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 0, sign=False)
        
        #enter actual min
        self._writeData(_ModbusCommands.MinCalibrSetP, -450, sign=True)
     
    
    async def setPumpPressure(self, pressure):        
        #set pressure chamber treshold
        if pressure in range(0, 550):
            self._writeData(_ModbusCommands.PressureThresholdmBar, pressure, sign=True)
        else:
            raise ValueError("Pressure not in range")

    async def setPumpVacuum(self, vacuum):
        #set vacuum chamber treshold
        if vacuum in range(0, -550):
            self._writeData(_ModbusCommands.VacuumThresholdmBar, vacuum, sign=True)
        else:
            raise ValueError("Vacuum not in range")

    async def aspirate(self, actuationTime: int, pressure: int):

        #set output pressure
        if pressure in range(-450, 0):
            self._writeData(_ModbusCommands.OutputPressuremBar, pressure)
        else:
            raise ValueError("Pressure not in range")
        await self._waitUntilPressureRegulated(pressure)
        #set actuation time
        if actuationTime in range(0, 1000):
            self._writeData(_ModbusCommands.ValveActuationTime, actuationTime, sign=False)
            await asyncio.sleep(actuationTime/1000)
        else:
            raise ValueError("Actuation time not in range")

    async def dispense(self, actuationTime: int, pressure: int):
        #convert from uL to actuation time
        if pressure in range(0, 450):
            self._writeData(_ModbusCommands.OutputPressuremBar, pressure)
        else:
            raise ValueError("Pressure not in range")
        await self._waitUntilPressureRegulated(pressure)        
        #set actuation time
        if actuationTime in range(0, 1000):
            self._writeData(_ModbusCommands.ValveActuationTime, actuationTime, sign=False)
            await asyncio.sleep(actuationTime/1000)
        else:
            raise ValueError("Actuation time not in range")

    def readSensData(self):

        self.sensorData['vaccumChamber'] = self._readData(_ModbusCommands.VacuumActualmBar, True)
        self.sensorData['pressureChamber'] = self._readData(_ModbusCommands.PressureActualmBar, True)
        self.sensorData['outputPressure'] = self._readData(_ModbusCommands.OutputPressureActualmBar, True)
    
        return self.sensorData

    async def _waitUntilPressureRegulated(self, desiredPressure:int):
        STABLE_PRESSURE_THRESHOLD = 3
        TIMEOUT = 100 # 100 * 50 ms = 5000 ms

        timesPressureReached = 0
        timeoutCounter = 0

        # If the desired pressure less than +-50 mbar we will wait until the
        # pressure is +- 10%. Otherwise, wait until the pressure is within +- 5%
        if (abs(desiredPressure) <= 50):
            margin = 0.1
        else:
            margin = 0.05

        maxAllowedDeviation = abs(int(desiredPressure * margin))
        
        while True:
            actualPressure = self._readData(_ModbusCommands.OutputPressureActualmBar, True)
            if (abs(desiredPressure - actualPressure) < maxAllowedDeviation):
                timesPressureReached += 1
            else:
                timesPressureReached = 0
            
            if (timesPressureReached == STABLE_PRESSURE_THRESHOLD):
                break
            
            if (timeoutCounter > TIMEOUT):
                raise PressureRegulationFailed("Failed to regulate the desired pressure!")

            timeoutCounter += 1

            await asyncio.sleep(0.05)

    def read_serial_number(self):
        PROD_NUMBER_LEN = 6
        prod_key = self.client.read_holding_registers(_ModbusCommands.ProductionNumber, PROD_NUMBER_LEN, unit=self.pgvaConfig.modbusSlave)
        
        # Convert the list of number to string after converting them to ASCII characters
        prod_key_bytes = []
        for reg in prod_key.registers:
            #TODO: Should the data be interpreted in this way?
            prod_key_bytes.append(reg // 100)
            prod_key_bytes.append(reg % 100)
            # prod_key_bytes.append((reg >> 8) & 0xFF)
            # prod_key_bytes.append(reg & 0xFF)

        return "".join([chr(x) for x in prod_key_bytes])

    def read_firmware_version(self):
        VERSION_LEN = 3
        version = self.client.read_input_registers(_ModbusCommands.FirmwareVer, VERSION_LEN, unit=self.pgvaConfig.modbusSlave)
        return ".".join([str(x) for x in version.registers])

if __name__ == '__main__':
    print("starting PGVA")
    pgva = None
    try:
        pgva= PgvaDriver(pgvaConfig(interface='serial', modbusSlave=16, baudrate=115200, comPort="/dev/ttyUSB0", ip="192.168.10.102", tcpPort=8502), logging)
    except:
        logging.error(f"FAILED {pgva}")

