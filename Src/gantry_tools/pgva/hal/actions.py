from dataclasses import dataclass, field
from typing import List
import uuid
from copy import deepcopy

@dataclass
class ActionParameter:
    name: str
    type: str
    show: bool

@dataclass
class Action:
    name: str
    description: str
    parameters: List[ActionParameter]
    _id: str = field(default_factory=uuid.uuid4)

ACTIONS = [
    Action(name="aspirate",
            description="Makes an aspiration with volume given in 'uLiters'",
            parameters=[ActionParameter(name="volume", type="float", show='true')]),
    Action(name="dispense",
            description="Makes a dispense with volume given in 'uLiters'",
            parameters=[ActionParameter(name="volume", type="float", show='true')]),
    Action(name="set_vacuum",
            description="Sets the threshold for the vacuum chamber",
            parameters=[ActionParameter(name="vacuum", type="float", show='true')]),
    Action(name="set_pressure",
            description="Sets the threshold for the pressure chamber",
            parameters=[ActionParameter(name="pressure", type="float", show='true')]),
    Action(name="pick_up_tip",
            description="Picks up a tip",
            parameters=[]),
    Action(name="drop_tip",
            description="drops a tip",
            parameters=[])            
]

def get_actions():
    actions_copy = deepcopy(ACTIONS)
    action_dict_list = [action.__dict__ for action in actions_copy]
    for action in action_dict_list:
        action['parameters'] = [parameter.__dict__ for parameter in action['parameters']]

    return action_dict_list
