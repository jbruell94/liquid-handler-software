from enum import unique
from typing import Dict, List
import asyncio
import uuid

from driver.pgva import PgvaDriver
from hal import PGVA_HAL_LOG
import config.pgva_config as pvConfig
from common.ExceptionTypes import PgvaNotAvailable
from hal.statusManager.statusDataTypes import PgvaStatus

class SimPgvaControl():
    """This class contains all the interfaces 
    that are needed to control the liquid opperations
    This includes aspirating, dispensing, mixing, tip ejections etc."""
    _instance = None
    _VERSION = "0.0.1"
    _NAME = "PGVA"
    _NO_ERROR_DESCRIPTION = "Normal operation"
    _has_error = False
    _error_description = _NO_ERROR_DESCRIPTION
    _ID = uuid.uuid4()

    def __new__(cls):
        if cls._instance is None:
            PGVA_HAL_LOG.info(f"Initilized Pgva Control class. One instance allowed")
            cls._instance = super(SimPgvaControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.pgvaModules = None
       
        try:
            self.toolConfig = pvConfig.get_pg_config()
            self.pgvaModules = list(map(lambda x: PgvaDriver(x, PGVA_HAL_LOG, connect=False), self.toolConfig.pgvaConfig))
        except ConnectionError:
            PGVA_HAL_LOG.error("Failed to connect to pressure generation unit")
    
    @property
    def version(self):
        return self._VERSION
        
    @property
    def id(self):
        return self._ID
    
    @property
    def name(self):
        return self._NAME

    @property
    def error_status(self):
        return self._has_error
    
    @property
    def tool_status(self):
        return self._error_description

    @property
    def dimensions(self):
        return self.toolConfig.dimensions.__dict__

    @property
    def referencePoint(self):
        return self.toolConfig.referencePoint

    def reload_config(self):
        SimPgvaControl.__init__(self)

    async def aspirate(self, volume: float, id: int = 1):
        """Makes an aspiration with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected

        @param: volume - volume to aspirate in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotPresent - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Aspirating from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                # Only check if the ID is valid
                _ = self._find_pgva_module(id)

                for _ in range(10):
                    await asyncio.sleep(0.1)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable

    
    async def dispense(self, volume: float, id: int = 1):
        """Makes a dispense with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected
        @param: volume - volume to dispense in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotPresent - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Dispensing from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                # Only check if the ID is valid
                _ = self._find_pgva_module(id)
                for _ in range(10):
                    await asyncio.sleep(0.1)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable


    def get_status_pg(self, id: int):
        #read the actual status of the pgva
        # e.g. self.pgvaModules[id].read_status()
        return PgvaStatus(id=id, status='Ready')

    def get_available_devices(self) -> List[Dict[str, str]]:
        available_devices = []
        for module in self.pgvaModules:
            try:
                unique_id = module.pgvaConfig.unique_id
                serial_number = "0123456789A"
                # fw_version = "1.2.3"
                available_devices.append({"id": unique_id, "serial_number": serial_number})
            except Exception as e:
                self._handle_pgva_exception(e)

        return available_devices

    def acknowledge_error(self):
        self._clear_error()

    def _find_pgva_module(self, pgva_id: int):
        try:
            # it is guaranteed that the ID is unique and there will not be more than 1 entry
            return list(filter(lambda x: x.pgvaConfig.unique_id == pgva_id, self.pgvaModules))[0]
        except IndexError:
            raise ValueError("No such PGVA module")

    def _set_error_state(self, error_desc):
        self._has_error = True
        self._error_description = error_desc

    def _clear_error(self):
        self._has_error = False
        self._error_description = self._NO_ERROR_DESCRIPTION

    def _handle_pgva_exception(self, exc):
        PGVA_HAL_LOG.error(f"Got exception: {exc.__class__.__name__}({exc})")
        self._set_error_state(str(exc))
        raise

if __name__ == "__main__":
    async def func():
        lq = SimPgvaControl()
        lq.aspirate(100, 1)
        lq.dispense(200, 1)


    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())