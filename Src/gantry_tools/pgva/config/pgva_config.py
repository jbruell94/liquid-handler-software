from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Dict, Any, List
import json
import jsonschema
from jsonschema import ValidationError
import pkg_resources
from copy import deepcopy

from config.data_types import Pgva, ToolConfig, Dimensions
from config import PGVA_CONFIG_FILE, PGVA_CONFIG_LOG

class PgvaEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ToolConfig):
            dict = deepcopy(obj.__dict__)
            dict['dimensions'] = dict["dimensions"].__dict__
            dict['pgvaConfig'] = [axis.__dict__ for axis in dict["pgvaConfig"]]
            return dict
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

DEF_X_DIMENSION = 100
DEF_Y_DIMENSION = 200
DEF_Z_DIMENSION = 50
DEF_REFERENCE_POINT = 'center'

DEF_PGVAS = [{}]

DEF_INTERFACE_PGVA = 'serial'
DEF_BAUDRATE_PGVA = 115200
DEF_IP_ADDRESS_PGVA = '192.168.0.118'
DEF_TCP_PORT_PGVA = 502
DEF_SERIAL_PORT_PGVA = "/dev/ttyUSB0"
DEF_MODBUS_SLAVE_PGVA = 16

def _build_pv_gen_config(pv_config: Dict[str, Any]) -> ToolConfig:
    config: Dict[str, Any]
    pgvasConfig = []
    counter = 0
    used_ids = set()

    dim = Dimensions(
        xDimension=pv_config.get('dimensions', {}).get('xDimension', DEF_X_DIMENSION),
        yDimension=pv_config.get('dimensions', {}).get('yDimension', DEF_Y_DIMENSION),
        zDimension=pv_config.get('dimensions', {}).get('zDimension', DEF_Z_DIMENSION)
        )

    referencePoint = pv_config.get('referencePoint', DEF_REFERENCE_POINT)
    devices = pv_config.get('pgvaConfig', {})

    for config in devices if devices else DEF_PGVAS:
        if 'unique_id' not in config:
            config['unique_id'] = counter

        if config['unique_id'] not in used_ids:
            used_ids.add(config['unique_id'])
            pgva = Pgva(unique_id=config['unique_id'],
                        interface=config.get('interface', DEF_INTERFACE_PGVA),
                        baudrate=config.get('baudrate', DEF_BAUDRATE_PGVA),
                        comPort=config.get('comPort', DEF_SERIAL_PORT_PGVA),
                        ip=config.get('ip', DEF_IP_ADDRESS_PGVA),
                        tcpPort=config.get('tcpPort', DEF_TCP_PORT_PGVA),
                        modbusSlave=config.get('modbusSlave', DEF_MODBUS_SLAVE_PGVA)
                        )
            pgvasConfig.append(pgva)
            counter += 1
        else:
            raise ValueError("IDs should be unique")

    return ToolConfig(dimensions=dim, referencePoint=referencePoint, pgvaConfig=pgvasConfig)


def _write_config(configs: List, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config
    
def get_pg_config() -> ToolConfig:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(PGVA_CONFIG_FILE)
    try:
        config = _build_pv_gen_config(json_config)
    except:
        PGVA_CONFIG_LOG.error("could not get the config file")
        return None
    _write_config(config, PGVA_CONFIG_FILE, PgvaEncoder)
    return config


def set_config(pgva_config: ToolConfig):
    """Writes the desired configuration to the config file
    """
    try:
        schema = pkg_resources.resource_string('config', 'pgva_schema.json').decode('utf8')
    except FileNotFoundError:
        PGVA_CONFIG_LOG.critical("Schema resource could not be loaded")
        raise

    try:
        schemaJson = json.loads(schema)
        jsonschema.validate(pgva_config, schemaJson)
        PGVA_CONFIG_LOG.info(f'Writing file {pgva_config}')    
        _write_config(pgva_config, PGVA_CONFIG_FILE, PgvaEncoder)
    except ValidationError:
        PGVA_CONFIG_LOG.error("Incompatible configuration")
        raise ValueError

        
if __name__ == "__main__":
    jsonConfig = _load_config(PGVA_CONFIG_FILE)
    pgva = _build_pv_gen_config(jsonConfig)
    _write_config(pgva, PGVA_CONFIG_FILE, PgvaEncoder)
